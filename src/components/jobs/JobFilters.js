import { Component } from 'react';
import Select from 'antd/lib/select';
import { ROLES } from 'src/helpers/roles';
import { EMPLOYMENT_TYPES } from 'src/helpers/employmentTypes';
import {AFRICAN_COUNTRIES, REMOTE} from 'src/helpers/africanCountries';
import styles from 'src/components/styles/JobFilters.module.css';

class JobFilters extends Component {
    state = {
        filters: {
            role: null,
            employmentType: null,
            country: null,
        },
    };

    handleSelectRole = (role) => {
        const filters = this.state.filters;
        filters.role = role || null;
        this.setState({
            filters
        }, () => {
            this.props.onChange(this.state.filters);
        });
    }

    getRoleFilter() {
        const roles = [];
        ROLES.forEach((role) => {
            roles.push(<Select.Option value={role} key={role}>{role}</Select.Option>);
        });

        return (
            <Select showSearch={true} size="large" className={styles.jobFilters__filterRoles} bordered={false} placeholder="Filter by role" allowClear={true} onChange={this.handleSelectRole}>
                {roles}
            </Select>
        );
    }

    handleSelectEmploymentType = (employmentType) => {
        const filters = this.state.filters;
        filters.employmentType = employmentType || null;
        this.setState({
            filters
        }, () => {
            this.props.onChange(this.state.filters);
        });
    }

    getEmploymentTypeFilter() {
        const employmentTypes = [];
        EMPLOYMENT_TYPES.forEach((employmentType) => {
            employmentTypes.push(<Select.Option value={employmentType} key={employmentType}>{employmentType}</Select.Option>);
        });

        return (
            <Select showSearch={true} size="large" className={styles.jobFilters__filterEmploymentType} bordered={false} placeholder="Filter by employment type" allowClear={true} onChange={this.handleSelectEmploymentType}>
                {employmentTypes}
            </Select>
        );
    }

    handleSelectCountry = (country) => {
        const filters = this.state.filters;
        filters.country = country || null;
        this.setState({
            filters
        }, () => {
            this.props.onChange(this.state.filters);
        });
    }

    getCountryFilter() {
        const countries = [];
        countries.push(<Select.Option value={REMOTE} key={REMOTE}>Remote</Select.Option>);
        AFRICAN_COUNTRIES.forEach((country) => {
            countries.push(<Select.Option value={country} key={country}>{country}</Select.Option>);
        });

        return (
            <Select showSearch={true} size="large" className={styles.jobFilters__filterCountry} bordered={false} placeholder="Filter by location" allowClear={true} onChange={this.handleSelectCountry}>
                {countries}
            </Select>
        );
    }

    render() {
        return (
            <div className={styles.jobFilters}>
                {this.props.showCountryFilter && <div className={styles.jobFilters__filter}>{this.getCountryFilter()}</div>}
                {this.props.showRoleFilter && <div className={styles.jobFilters__filter}>{this.getRoleFilter()}</div>}
                {this.props.showEmploymentTypeFilter && <div className={styles.jobFilters__filter}>{this.getEmploymentTypeFilter()}</div>}
            </div>
        );
    }
}

JobFilters.defaultProps = {
    showCountryFilter: true,
    showRoleFilter: true,
    showEmploymentTypeFilter: true,
}

export default JobFilters;
