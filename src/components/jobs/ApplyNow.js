import React from 'react';
import Button from 'antd/lib/button';
import { LinkOutlined, MailOutlined, CloseOutlined } from "@ant-design/icons";
import { getRootUrl, isEmail } from 'src/helpers/helpers';

const ApplyNow = ({ jobId, applicationMethod, isInactive }) => {
    if (isInactive) {
        return (
            <div>
                <Button size="large" block={true} disabled={true}>
                    <CloseOutlined /> This job is no longer active
                </Button>
            </div>
        );
    }

    let applicationLink = applicationMethod;
    const isEmailApplicationMethod = isEmail(applicationLink);

    if (!isEmailApplicationMethod) {
        let url = new URL(applicationLink);

        url.searchParams.append('source', 'launchafrica.io');

        applicationLink = url.toString();
    }

    const handleApply = async () => {
        if (process.env.NODE_ENV !== 'development') {
            await fetch(`${getRootUrl()}/api/job/${jobId}/incrementApplies`);
        }
    }

    return (
        <div>
            <Button onClick={handleApply} size="large" rel="noopener" type="primary" block={true} href={isEmailApplicationMethod ? `mailto:${applicationLink}` : applicationLink} target="_blank">
                <strong>
                    {!isEmailApplicationMethod && <><LinkOutlined/> APPLY NOW</>}
                    {isEmailApplicationMethod && <><MailOutlined/> APPLY NOW via EMAIL</>}
                </strong>
            </Button>
        </div>
    );
};

export default ApplyNow;
