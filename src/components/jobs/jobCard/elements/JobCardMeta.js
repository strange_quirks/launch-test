import React from 'react';
import { EnvironmentOutlined, LaptopOutlined, ClockCircleOutlined, UserOutlined } from '@ant-design/icons';
import styles from 'src/components/styles/JobCard.module.css';
import { REMOTE } from "src/helpers/africanCountries";

const JobCardMeta = ({ job }) => {
    const location = [];
    if (job.country) {
        location.push(<span key="country"><EnvironmentOutlined/> {job.city ? `${job.city}, ` : ''}<span className={styles.jobCard__metaCountry}>{job.country}</span></span>);
    }
    if (job.countryList.includes(REMOTE)) {
        if (location.length === 1) {
            location.push(<span key="middot"> &middot; </span>);
        }
        location.push(<span key="remote"><LaptopOutlined/> Remote</span>);
    }

    return (
        <div>
            <div className={styles.jobCard__metaRow}>
                <div key="location" className={styles.jobCard__metaItem} title="Location">
                    {location}
                </div>
            </div>
            <div className={styles.jobCard__metaRow}>
                <div className={styles.jobCard__metaItem} title="Role">
                    <UserOutlined/> {job.role}
                </div>
                {job.employmentType && <div className={styles.jobCard__metaItem} title="Employment type">
                    <ClockCircleOutlined/> {job.employmentType}
                </div>}
            </div>
        </div>
    );
};

export default JobCardMeta;
