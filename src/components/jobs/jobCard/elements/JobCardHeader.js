import React from 'react';
import styles from 'src/components/styles/JobCard.module.css';

const JobCardHeader = ({ job }) => (
    <div className={styles.jobCard__header}>
        <div className={styles.jobCard__headerImage}>
            <img
                loading="lazy"
                width="80"
                src={job.companyLogo.imageUrl}
                alt={`${job.companyName} jobs`}
            />
        </div>
        <div className={styles.jobCard__headerJob}>
            <div className={styles.jobCard__headerCompany}>
                {job.companyName}
            </div>
            <div className={styles.jobCard__headerTitle}>
                {job.title}
            </div>
        </div>
    </div>
);

export default JobCardHeader;
