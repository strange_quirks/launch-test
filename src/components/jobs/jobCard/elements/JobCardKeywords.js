import React from 'react';
import Tag from 'antd/lib/tag';
import styles from 'src/components/styles/JobCard.module.css';

const JobCardKeywords = ({ job }) => {
    if (!job.keywords || job.keywords.length === 0) {
        return null;
    }

    const keywords = job.keywords.map((keyword) => <Tag key={keyword} className={styles.jobCard__keywordsTag}>{keyword}</Tag>);
    return (
        <div className={styles.jobCard__keywords}>
            {keywords}
        </div>
    );
};

export default JobCardKeywords;
