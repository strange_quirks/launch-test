import { Component } from 'react';
import { withRouter } from 'next/router';
import JobCardHeader from "src/components/jobs/jobCard/elements/JobCardHeader";
import JobCardMeta from "src/components/jobs/jobCard/elements/JobCardMeta";
import JobCardKeywords from "src/components/jobs/jobCard/elements/JobCardKeywords";
import Link from 'next/link';
import { getJobUrl, secondsToDateTime } from 'src/helpers/helpers';
import styles from 'src/components/styles/JobCard.module.css';
import Badge from 'antd/lib/badge';
import { differenceInCalendarDays } from 'date-fns'
import { STATUS_DELETED, STATUS_EXPIRED } from "src/helpers/constants";

class JobCard extends Component {
    state = {
        currentPathname: this.props.router.asPath,
        detailsUrl: getJobUrl(this.props.job.title, this.props.job.id),
    }

    render() {
        let { job } = this.props;

        let jobContent = (
            <Link href={this.state.detailsUrl}>
                <a title={job.title} target="_blank" rel="noopener">
                    <div className={styles.jobCard}>
                        <JobCardHeader job={job} />
                        <JobCardMeta job={job} />
                        <JobCardKeywords job={job} />
                    </div>
                </a>
            </Link>
        );

        let createdAtDate;
        if (job.createdAt.seconds) {
            createdAtDate = secondsToDateTime(job.createdAt.seconds);
        } else {
            createdAtDate = new Date(job.createdAt);
        }
        const daysOnline = differenceInCalendarDays(new Date(), createdAtDate);
        if (daysOnline <= 8) {
            jobContent = (
                <Badge.Ribbon text="New" color="blue">
                    {jobContent}
                </Badge.Ribbon>
            );
        } else if (daysOnline >= 50 && daysOnline <= 60) {
            jobContent = (
                <Badge.Ribbon text="Expiring soon" color="gold">
                    {jobContent}
                </Badge.Ribbon>
            );
        } else if ([STATUS_DELETED, STATUS_EXPIRED].includes(job.status)) {
            jobContent = (
                <Badge.Ribbon text="Expired" color="red">
                    {jobContent}
                </Badge.Ribbon>
            );
        }

        return jobContent;
    }
}

export default withRouter(JobCard);
