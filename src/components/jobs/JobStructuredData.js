import { Component } from 'react';
import { format } from "date-fns";
const edjsHTML = require("editorjs-html");

class JobStructuredData extends Component {
    makeJobSchema(job) {
        const edjsParser = edjsHTML();
        let html = edjsParser.parse(JSON.parse(job.description));

        return {
            '@context': 'http://schema.org',
            '@type': 'JobPosting',
            datePosted: format(new Date(job.createdAt), 'YYY-MM-dd'),
            description: html,
            title: job.title,
            image: job.companyLogo.imageUrl,
            employmentType: job.employmentType,
            validThrough: format(new Date(job.endDate), 'YYY-MM-dd'),
            hiringOrganization: {
                '@type': 'Organization',
                name: job.companyName,
                sameAs: job.companyWebsite || null,
                logo: job.companyLogo.imageUrl,
            },
            jobLocation: {
                "@type": "Place",
                "address": {
                    "@type": "PostalAddress",
                    "addressCountry": job.country,
                }
            },
        }
    }

    render() {
        return (
            <script
                key={`jobJSON-${this.props.job.id}`}
                type='application/ld+json'
                dangerouslySetInnerHTML={{ __html: JSON.stringify(this.makeJobSchema(this.props.job)) }}
            />
        )
    }
}

export default JobStructuredData;
