import { Component } from 'react';
import JobCard from "src/components/jobs/jobCard/JobCard";
import Empty from 'antd/lib/empty';
import Observer from '@researchgate/react-intersection-observer';
import styles from 'src/components/styles/JobCardList.module.css';

class JobCardList extends Component {
    render() {
        const jobCards = [];
        this.props.jobs.forEach((job) => {
            jobCards.push(<JobCard key={job.id} job={job} />);
        });
        if (!jobCards || jobCards.length === 0) {
            return (
                <div className={styles.jobsListEmpty}>
                    <Empty image={Empty.PRESENTED_IMAGE_DEFAULT} />
                </div>
            );
        }

        const options = {
            onChange: () => {
               this.props.onLoadMore();
            },
            threshold: 1,
            // root: '#scrolling-container',
            // rootMargin: '0% 0% -25%',
        };

        return (
            <div className={styles.jobsList}>
                {jobCards}
                <Observer {...options}>
                    <div />
                </Observer>
            </div>
        );
    }
}

export default JobCardList;
