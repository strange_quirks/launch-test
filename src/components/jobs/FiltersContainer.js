import { Component } from 'react';
import Typography from "antd/lib/typography";
import Button from "antd/lib/button";
import {FilterOutlined} from "@ant-design/icons";
import JobFilters from "src/components/jobs/JobFilters";
import classNames from "classnames";
import styles from 'src/components/styles/FiltersContainer.module.css';
import * as PropTypes from "prop-types";

class FiltersContainer extends Component {
    state = {
        filtersVisible: false,
    };

    render() {
        const classNamesObject = {};
        classNamesObject[styles.indexContainer__filters] = true;
        classNamesObject[styles.indexContainer__filtersVisible] = this.state.filtersVisible;
        const filtersContainerClassNames = classNames(classNamesObject);

        return (
            <div className={styles.indexContainer__latest}>
                <div className={styles.indexContainer__latestTitle}>
                    <Typography.Title level={2} id="latest">Latest jobs</Typography.Title>
                    <Button role="button" aria-pressed="false" className={styles.indexContainer__filterIcon} icon={<FilterOutlined />} size="large" onClick={() => this.setState({ filtersVisible: !this.state.filtersVisible})}/>
                </div>
                <div className={filtersContainerClassNames}>
                    <JobFilters
                        showCountryFilter={this.props.showCountryFilter}
                        showRoleFilter={this.props.showRoleFilter}
                        showEmploymentTypeFilter={this.props.showEmploymentTypeFilter}
                        onChange={this.props.onUpdate}
                    />
                </div>
            </div>
        );
    }
}

FiltersContainer.propTypes = {
    onUpdate: PropTypes.func.isRequired,
    showCountryFilter: PropTypes.bool,
    showRoleFilter: PropTypes.bool,
    showEmploymentTypeFilter: PropTypes.bool,
}

FiltersContainer.defaultProps = {
    showCountryFilter: true,
    showRoleFilter: true,
    showEmploymentTypeFilter: true,
}

export default FiltersContainer;
