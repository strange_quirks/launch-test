import { Component } from 'react';
import Typography from 'antd/lib/typography';
import Skeleton from 'antd/lib/skeleton';
import List from 'antd/lib/list';
import { getJobUrl } from "src/helpers/helpers";
import Link from "next/link";
import { ShopOutlined, LaptopOutlined, EnvironmentOutlined } from '@ant-design/icons';
import styles from 'src/components/styles/SimilarJobs.module.css';
import { REMOTE } from "src/helpers/africanCountries";

class SimilarJobs extends Component {
    state = {
        jobs: null,
    }

    async firestoreGetJobs(role) {
        const url = `/api/job/${this.props.currentJobId}/similar`
        const result = await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                role: role,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return await result.json();
    }

    componentDidMount() {
        this.firestoreGetJobs(this.props.role).then((jobs) => {
            this.setState({
                jobs: jobs.filter((job) => job.id !== this.props.currentJobId)
            })
        })
    }

    shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    render() {
        const { jobs } = this.state;
        if (!jobs) {
            return (
                <>
                    <Skeleton active={true} paragraph={{ rows: 1 }} />
                    <Skeleton active={true} paragraph={{ rows: 1 }} />
                    <Skeleton active={true} paragraph={{ rows: 1 }} />
                </>
            );
        }

        if (jobs.length === 0) {
            return null;
        }

        this.shuffleArray(jobs);

        return (
            <div className={styles.similarJobs}>
                <Typography.Title level={5}>Similar jobs</Typography.Title>
                <List
                    itemLayout="vertical"
                    dataSource={jobs.slice(0, 6)}
                    renderItem={(job) => {
                        const meta = [
                            <span key="company"><ShopOutlined /> {job.companyName}</span>,
                        ];
                        if (job.country) {
                            meta.push(<span key="countrySep"> | </span>);
                            meta.push(<span key="country"><EnvironmentOutlined /> {job.country}</span>);
                        }
                        if (job.countryList.includes(REMOTE)) {
                            meta.push(<span key="remoteSep"> | </span>);
                            meta.push(<span key="remote"><LaptopOutlined /> Remote</span>);
                        }

                        return (
                            <List.Item key={job.id}>
                                <Link href={getJobUrl(job.title, job.id)}>
                                    <a>
                                        {job.title}
                                    </a>
                                </Link>
                                <div>
                                    <Typography.Text>{meta}</Typography.Text>
                                </div>
                            </List.Item>
                        );
                    }}
                />
            </div>
        );
    }
}

export default SimilarJobs;
