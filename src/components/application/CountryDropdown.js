import React from 'react';
import Menu from 'antd/lib/menu';
import Dropdown from 'antd/lib/dropdown';
import Button from 'antd/lib/button';
import { DownOutlined } from '@ant-design/icons';
import {AFRICAN_COUNTRIES, REMOTE} from 'src/helpers/africanCountries';
import Link from "next/link";
import { getCountryUrl } from 'src/helpers/helpers';

const CountryDropdown = ( ) => {
    const menuItems = [];
    menuItems.push(
        <Menu.Item key={REMOTE}>
            <Link href={getCountryUrl(REMOTE)}>Remote</Link>
        </Menu.Item>
    );
    AFRICAN_COUNTRIES.forEach((country) => {
        menuItems.push(
            <Menu.Item key={country}>
                <Link href={getCountryUrl(country)}>{country}</Link>
            </Menu.Item>
        );
    });

    const menu = (
        <Menu>
            {menuItems}
        </Menu>
    );

    return (
        <Dropdown overlay={menu} overlayClassName="country-dropdown-overlay" trigger="click">
            <Button type="text" size="large">
                Location <DownOutlined/>
            </Button>
        </Dropdown>
    );
};

export default CountryDropdown;
