import React from 'react';
import Button from 'antd/lib/button';
import Divider from 'antd/lib/divider';
import { FormOutlined } from "@ant-design/icons";
import Link from "next/link";
import {getAboutUrl, getPostJobUrl} from 'src/helpers/helpers';
import CategoryDropdown from "src/components/application/CategoryDropdown";
import CountryDropdown from "src/components/application/CountryDropdown";
import MenuDrawer from "src/components/application/MenuDrawer";
import styles from 'src/components/styles/Menu.module.css';

function Menu() {
    return (
        <div>
            <div className={styles.menu__mobile}>
                <MenuDrawer />
            </div>
            <div className={styles.menu__default}>
                <span>
                    <Link href={getAboutUrl()}>
                        <a href={getAboutUrl()} aria-label="About" className={styles.menu__about}>
                            About us
                        </a>
                    </Link>
                </span>
                <Divider type="vertical" />
                <span>
                    <CountryDropdown />
                </span>
                <Divider type="vertical" />
                <span>
                    <CategoryDropdown />
                </span>
                <Link href={getPostJobUrl()}>
                    <Button href={getPostJobUrl()} size="large" type="primary" className={styles.menu__action} aria-label="Post a job for free">
                        <FormOutlined /> Post a job for free
                    </Button>
                </Link>
            </div>
        </div>
    );
}

export default Menu;
