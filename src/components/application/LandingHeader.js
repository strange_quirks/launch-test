import { Component } from 'react';
import Typography from 'antd/lib/typography';
import Input from 'antd/lib/input';
import Link from "next/link";
import Menu from "src/components/application/Menu";
import styles from 'src/components/styles/LandingHeader.module.css';
import Button from "antd/lib/button";
import { ArrowRightOutlined } from "@ant-design/icons";
import { SearchOutlined } from "@ant-design/icons";
import { withRouter } from "next/router";

class LandingHeader extends Component {
    render() {
        return (
            <div className={styles.landingHeader}>
                <div className={styles.landingHeader__content}>
                    <div>
                        <div className={styles.landingHeader__topContent}>
                            <div>
                                <Link href="/">
                                    <a href="/"><img loading="lazy" height="40" src="/images/logo.png" alt="LaunchAfrica logo" className={styles.landingHeader__logo}/></a>
                                </Link>
                            </div>
                            <div>
                                <Menu />
                            </div>
                        </div>
                    </div>
                    <div className={styles.landingHeader__main}>
                        <div>
                            <Typography.Title className={styles.landingHeader__headline}>
                                Startup and Tech Jobs<br/>in Africa
                            </Typography.Title>
                            <div className={styles.landingHeader__actions}>
                                <Button type="primary" aria-label="Browse jobs" size="large" href="#latest" className={styles.landingHeader__actionBrowse}>
                                    <SearchOutlined /> Browse jobs
                                </Button>
                                <div className={styles.landingHeader__actionHiring}>
                                    Hiring? <Link href="/post/job">
                                        <a href="/post/job">
                                            Post a job for free <ArrowRightOutlined />
                                        </a>
                                    </Link>
                                </div>
                            </div>
                            <div className={styles.landingHeader__search}>
                                <Input.Search placeholder="Start searching. e.g. JavaScript, Marketing, Zambia, ..." size="large" onSearch={(value) => {
                                    if (value) {
                                        this.props.router.push(`/search/${value}`);
                                    }
                                }} enterButton={true} />
                            </div>
                        </div>
                        <div className={styles.landingHeader__image}>
                            <picture>
                                <source srcSet="/images/africa.webp" type="image/webp" />
                                <source srcSet="/images/africa.png" type="image/png" />
                                <img src="/images/africa.webp" type="image/webp" alt="Startup jobs in Africa" className={styles.landingHeader__imageImg} />
                            </picture>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(LandingHeader);
