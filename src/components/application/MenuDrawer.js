import React, { useState } from 'react';
import Collapse from 'antd/lib/collapse';
import Drawer from 'antd/lib/drawer';
import Button from 'antd/lib/button';
import Divider from 'antd/lib/divider';
import {FormOutlined, MenuOutlined, QuestionCircleOutlined, TwitterOutlined } from "@ant-design/icons";
import Link from "next/link";
import { getPostJobUrl, getCategoryUrl, getCountryUrl } from 'src/helpers/helpers';
import { ROLES } from 'src/helpers/roles';
import {AFRICAN_COUNTRIES, REMOTE} from 'src/helpers/africanCountries';
import styles from 'src/components/styles/MenuDrawer.module.css';

function MenuDrawer() {
    const [visible, setVisible] = useState(false);
    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };

    const categories = [];
    ROLES.forEach((role) => {
        categories.push(
            <Link href={getCategoryUrl(role)} key={role}>
                <Button type="link" aria-label={role}>
                    {role}
                </Button>
            </Link>
        );
    });

    const countries = [];
    countries.push(
        <Link href={getCountryUrl(REMOTE)} key={REMOTE}>
            <Button type="link" aria-label="Remote">
                Remote
            </Button>
        </Link>
    );
    AFRICAN_COUNTRIES.forEach((country) => {
        countries.push(
            <Link href={getCountryUrl(country)} key={country}>
                <Button type="link" aria-label={country}>
                    {country}
                </Button>
            </Link>
        );
    });

    return (
        <div className="menu-drawer">
            <Button onClick={showDrawer} icon={<MenuOutlined />} aria-label="Menu"/>
            <Drawer
                title="LaunchAfrica"
                placement="right"
                closable={true}
                onClose={onClose}
                visible={visible}
            >
                <div className={styles.menuDrawer__actions}>
                    <Link href={getPostJobUrl()}>
                        <Button href={getPostJobUrl()} size="large" type="primary" block={true} aria-label="Post a job for free">
                            <FormOutlined /> Post a job for free
                        </Button>
                    </Link>
                    <Button
                        className={styles.menuDrawer__actionsFeedback}
                        block={true}
                        size="large"
                        icon={<span><QuestionCircleOutlined /></span>}
                        href="https://qrfeedbackcreator.com/form/LguXLR6ko"
                        target="_blank"
                        rel="noopener"
                    >&nbsp;Feedback</Button>
                </div>
                <Divider orientation="left" />
                <Collapse ghost={true} accordion={true} expandIconPosition="right">
                    <Collapse.Panel header="Categories" key="1">
                        {categories}
                    </Collapse.Panel>
                    <Collapse.Panel header="Location" key="2">
                        {countries}
                    </Collapse.Panel>
                </Collapse>
                <Divider orientation="left" />
                <div>
                    &copy; LaunchAfrica {(new Date()).getFullYear()} &middot; <Link href="/privacy" aria-label="Privacy">
                    <a href="/privacy">Privacy</a></Link> &middot; <Link href="/about" aria-label="About">
                    <a href="/about">About us</a>
                </Link> &middot; <a href="https://twitter.com/africa_launch" target="_blank" rel="noopener"><TwitterOutlined /> Follow us on Twitter</a>
                </div>
            </Drawer>
        </div>
    );
}

export default MenuDrawer;
