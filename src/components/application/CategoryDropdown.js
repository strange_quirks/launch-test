import React from 'react';
import Menu from 'antd/lib/menu';
import Dropdown from 'antd/lib/dropdown';
import Button from 'antd/lib/button';
import { DownOutlined } from '@ant-design/icons';
import { ROLES } from 'src/helpers/roles';
import Link from "next/link";
import { getCategoryUrl } from 'src/helpers/helpers';

const CategoryDropdown = ( ) => {
    const menuItems = [];
    ROLES.forEach((role) => {
        menuItems.push(
            <Menu.Item key={role}>
                <Link href={getCategoryUrl(role)}>{role}</Link>
            </Menu.Item>
        );
    });

    const menu = (
        <Menu>
            {menuItems}
        </Menu>
    );

    return (
        <Dropdown overlay={menu} trigger="click">
            <Button type="text" size="large">
                Categories <DownOutlined/>
            </Button>
        </Dropdown>
    );
};

export default CategoryDropdown;
