import React from 'react';
import Typography from 'antd/lib/typography';
import Link from "next/link";
import { getCompanyUrlByKey } from 'src/helpers/helpers';
import styles from 'src/components/styles/CompanySpotlight.module.css';

const CompanySpotlightItem = (props) => {
    return (
        <Link href={getCompanyUrlByKey(props.keyProp)}>
            <a>
                <div className={styles.companySpotlightItem} title={props.name}>
                    <div className={styles.companySpotlightItem__imageContainer}>
                        <img src={props.logo.imageUrl} alt={`${props.name} jobs logo`} />
                    </div>
                    <div className={styles.companySpotlightItem__name}>
                        <Typography>
                            <strong>{props.name}</strong>
                        </Typography>
                    </div>
                </div>
            </a>
        </Link>
    );
};

export default CompanySpotlightItem;
