import { Component } from 'react';
import Typography from 'antd/lib/typography';
import CompanySpotlightItem from "src/components/company/CompanySpotlightItem";
import styles from 'src/components/styles/CompanySpotlight.module.css';
import { getRootUrl } from "src/helpers/helpers";

class CompanySpotlight extends Component {
    state = {
        companies: this.props.companies,
    };

    async componentDidMount() {
        const responseJobs = await fetch(`${getRootUrl()}/api/companies/find`, {
            method: 'POST',
            body: JSON.stringify({
                limit: 6,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const data = await responseJobs.json();
        this.setState({
            companies: data,
        })
    }

    render() {
        if (!this.state.companies) {
            return null;
        }

        const companyItems = [];
        this.state.companies.forEach((company) => {
            companyItems.push(<CompanySpotlightItem key={company.id} keyProp={company.key} {...company} />);
        });
        if (!companyItems || companyItems.length === 0) {
            return null;
        }

        return (
            <div className={styles.companySpotlight}>
                <Typography.Title level={2}>Latest companies</Typography.Title>
                <div className={styles.companySpotlight__items}>
                    {companyItems}
                </div>
            </div>
        );
    }
}

export default CompanySpotlight;
