import React from 'react';
import Select from 'antd/lib/select';
import Form from 'antd/lib/form';
import { EMPLOYMENT_TYPES } from 'src/helpers/employmentTypes';
import styles from 'src/components/styles/PostJob.module.css';

const EmploymentTypeSelectFormItem = () => {
    const employmentTypes = [];
    EMPLOYMENT_TYPES.forEach((role) => {
        employmentTypes.push(<Select.Option value={role} key={role}>{role}</Select.Option>);
    });

    return (
        <Form.Item name="employmentType" label={<strong>Employment type</strong>} className={styles.postJobForm__employmentTypeFormItem}>
            <Select
                placeholder="Select an employment type"
            >
                {employmentTypes}
            </Select>
        </Form.Item>
    );
};

export default EmploymentTypeSelectFormItem;
