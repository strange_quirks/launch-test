import React from 'react';
import Typography from 'antd/lib/typography';
import Image from 'next/image';
import styles from 'src/components/styles/CompanyHeader.module.css';

const CompanyHeader = ({ company }) => {
    let url = new URL(company.website);
    url.searchParams.append('source', 'launchafrica.io');

    // <img src={company.logo.imageUrl} style={{width: '100%'}} alt={`${company.name} jobs logo`} />
    // <Image src={company.logo.imageUrl} layout="fill" />
    return (
        <div className={styles.companyHeader}>
            <a href={url.toString()} target="_blank" rel="noopener">
                <div className={styles.companyHeader__imageContainer}>
                    <img src={company.logo.imageUrl} style={{width: '100%'}} alt={`${company.name} jobs logo`} />
                </div>
            </a>
            <Typography.Title>
                <div className={styles.companyHeader__name}>
                    <a href={url.toString()} target="_blank" rel="noopener">
                        {company.name}
                    </a>
                </div>
            </Typography.Title>
            <div className={styles.companyHeader__description}>
                <Typography>
                    {company.description}
                </Typography>
            </div>
        </div>
    );
};

export default CompanyHeader;
