import { Component } from 'react';
import Typography from 'antd/lib/typography';
import { ROLES } from 'src/helpers/roles';
import CategoryItem from "src/components/common/CategoryItem";
import styles from 'src/components/styles/Categories.module.css';

class Categories extends Component {
    render() {
        const items = [];
        ROLES.forEach((role) => {
            items.push(<CategoryItem key={role} name={role} />);
        });

        return (
            <div className={styles.categories}>
                <Typography.Title level={2}>Categories</Typography.Title>
                <div className={styles.categories__items}>
                    {items}
                </div>
            </div>
        );
    }
}

export default Categories;
