import React from 'react';
import Select from 'antd/lib/select';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import { CURRENCIES } from 'src/helpers/currencies';

const SalaryFormItem = ({ onCurrencySelect }) => {
    const currencies = [];
    CURRENCIES.forEach((currency) => {
        currencies.push(<Select.Option value={currency} key={currency}>{currency}</Select.Option>);
    });

    const selectBefore = (
        <Form.Item
            name="currency"
            noStyle={true}
        >
            <Select onChange={onCurrencySelect}>
                {currencies}
            </Select>
        </Form.Item>
    );

    return (
        <Form.Item
            name="baseSalary"
            label={<strong>Annual base salary</strong>}
            rules={[{required: false}]}
            extra="Not required but highly recommended especially when searching for top applicants. Also helps a lot with SEO. Select a currency and then enter the base annual salary. If the salary is not paid annually, please convert it to an annual salary. We will let applicants know that this is just an estimated base salary on the job details page."
        >
            <Input addonBefore={selectBefore} placeholder="e.g. 50000 (optional)" type="number" />
        </Form.Item>
    );
};

export default SalaryFormItem;
