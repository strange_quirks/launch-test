import React from 'react';
import Select from 'antd/lib/select';
import Form from 'antd/lib/form';
import { AFRICAN_COUNTRIES } from 'src/helpers/africanCountries';

const CountrySelectFormItem = ({ onChange }) => {
    const countries = [];
    AFRICAN_COUNTRIES.forEach((country) => {
        countries.push(<Select.Option value={country} key={country}>{country}</Select.Option>);
    });

    return (
        <Form.Item
            name="country"
            label={<strong>Country</strong>}
            style={{display: 'inline-block', width: 'calc(50% - 10px)'}}
        >
            <Select
                allowClear={true}
                showSearch={true}
                placeholder="Select a country"
                onChange={onChange}
            >
                {countries}
            </Select>
        </Form.Item>
    );
};

export default CountrySelectFormItem;
