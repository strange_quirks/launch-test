import React from 'react';
import Typography from 'antd/lib/typography';
import styles from 'src/components/styles/CategoryHeader.module.css';

const KeywordHeader = ({ keyword }) => (
    <div className= {styles.categoryHeader}>
        <div>
            <Typography.Title className={styles.categoryHeader__headline}>
                <div className={styles.categoryHeader__category}>{keyword}</div>
            </Typography.Title>
            <Typography.Title level={2}>jobs in Africa</Typography.Title>
        </div>
        <div>
            <div className={styles.categoryHeader__image}>
                <picture>
                    <source srcSet="/images/africa.webp" type="image/webp" />
                    <source srcSet="/images/africa.png" type="image/png" />
                    <img src="/images/africa.webp" type="image/webp" alt="Startup jobs in Africa" className={styles.categoryHeader__imageImg} />
                </picture>
            </div>
        </div>
    </div>
);

export default KeywordHeader;
