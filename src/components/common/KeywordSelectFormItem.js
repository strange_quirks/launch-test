import React from 'react';
import Select from 'antd/lib/select';
import Form from 'antd/lib/form';

const KeywordSelectFormItem = ({ form }) => {
    const handleSelect = () => {
        form.setFieldsValue({
            keywords: form.getFieldValue('keywords').slice(0, 10),
        });
    };

    return (
        <Form.Item
            name="keywords"
            label={<strong>Keywords</strong>}
            rules={[{required: true}]}
            extra="Select up to 10 keywords. Press 'enter' to add the keyword."
        >
            <Select
                mode="tags"
                maxTagTextLength={20}
                placeholder="e.g. JavaScript, Engineer, QA, Marketing, Customer Success, etc."
                notFoundContent={<div>Enter a keyword. Press enter to select it.</div>}
                onSelect={handleSelect}
            />
        </Form.Item>
    );
};

export default KeywordSelectFormItem;
