import React, { Component } from 'react';
import EditorJS from '@editorjs/editorjs';

class Editor extends Component {
    state = {
        editor: null,
    };

    render() {
        if (!this.state.editor) {
            const Paragraph = require("@editorjs/paragraph");
            const List = require('@editorjs/list');
            const Marker = require('@editorjs/marker');
            const SimpleImage = require('@editorjs/simple-image');
            const InlineCode = require('@editorjs/inline-code');
            const Header = require('@editorjs/header');
            const Underline = require('@editorjs/underline');

            const editor = new EditorJS({
                data: this.props.data,
                holder: 'editorjs',
                placeholder: 'Click here to start creating the job description...',
                tools: {
                    header: {
                        class: Header,
                        shortcut: 'CMD+SHIFT+H',
                        config: {
                            levels: [2, 3, 4],
                            defaultLevel: 2
                        }
                    },
                    underline: {
                        class: Underline,
                        shortcut: 'CMD+SHIFT+U',
                    },
                    paragraph: {
                        class: Paragraph,
                        inlineToolbar: true,
                    },
                    list: {
                        class: List,
                        inlineToolbar: true,
                    },
                    image: {
                        class: SimpleImage,
                        inlineToolbar: true,
                        config: {
                            placeholder: 'Paste image URL'
                        }
                    },
                    Marker: {
                        class: Marker,
                        shortcut: 'CMD+SHIFT+M',
                    },
                    inlineCode: {
                        class: InlineCode,
                        shortcut: 'CMD+SHIFT+M',
                    },
                },
                onChange: () => {
                    editor.save().then((outputData) => {
                        this.props.onChange(outputData);
                    }).catch((error) => {
                        console.log('Saving failed: ', error)
                    });
                }
            });
            editor.isReady
                .then(() => {
                    this.setState({
                        editor,
                    });
                })
                .catch((reason) => {
                    console.log(`Editor.js initialization failed because of ${reason}`)
                });
        }

        return (
            <div className="editor">
                <div id="editorjs" />
            </div>
        );
    }
}

export default Editor;
