import React from 'react';
import Typography from 'antd/lib/typography';
import styles from 'src/components/styles/CountryKeywordDetails.module.css';

const CountryKeywordHeader = ({ country, keyword }) => (
    <div className={styles.countryKeywordHeader}>
        <div>
            <Typography.Title className={styles.categoryHeader__headline}>
                <div className={styles.countryKeywordHeader__category}>{keyword}</div> jobs in {country}
            </Typography.Title>
        </div>
        <div>
            <div className={styles.countryKeywordHeader__image}>
                <picture>
                    <source srcSet="/images/africa.webp" type="image/webp" />
                    <source srcSet="/images/africa.png" type="image/png" />
                    <img src="/images/africa.webp" type="image/webp" alt="Startup jobs in Africa" className={styles.countryKeywordHeader__imageImg} />
                </picture>
            </div>
        </div>
    </div>
);

export default CountryKeywordHeader;
