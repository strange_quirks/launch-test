import React from 'react';
import Output from 'editorjs-react-renderer';

const EditorRenderer = ({ description }) => {
    const style = {
        header: {
            fontFamily: `'FireSans',sans-serif`,
        },
        list: {
            fontFamily: `'FireSans',sans-serif`,
        },
        paragraph: {
            fontFamily: `'FireSans',sans-serif`,
        },
        Marker: {
            fontFamily: `'FireSans',sans-serif`,
        },
        underline: {
            fontFamily: `'FireSans',sans-serif`,
        },
    };

    return (
        <div className="editor">
            <Output
                data={description}
                style={style}
            />
        </div>
    );
};

export default EditorRenderer;
