import React from 'react';
import Typography from 'antd/lib/typography';
import Link from "next/link";
import { getCategoryUrl } from 'src/helpers/helpers';
import { ROLE_ICON_NAME_MAPPINGS } from 'src/helpers/roles';
import styles from 'src/components/styles/CategoryItem.module.css';

const CategoryItem = (props) => {
    return (
        <Link href={getCategoryUrl(props.name)}>
            <a className={styles.categoryItem}>
                <div title={`${props.name} jobs in Africa`} className={styles.categoryItem__container}>
                    <div className={styles.categoryItem__imageContainer}>
                        <img className={styles.categoryItem__image} src={`/icons/${ROLE_ICON_NAME_MAPPINGS[props.name]}.png`} alt={`${props.name} startup & tech jobs in Africa`} />
                    </div>
                    <div>
                        <Typography>
                            <strong  className={styles.categoryItem__categoryName}>{props.name}</strong>
                        </Typography>
                    </div>
                </div>
            </a>
        </Link>
    );
};

export default CategoryItem;
