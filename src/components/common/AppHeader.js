import React, { Component } from 'react';
import Link from 'next/link';
import Menu from "src/components/application/Menu";

class AppHeader extends Component {
    render() {
        return (
            <div className="app-header">
                <div className="app-header__content">
                    <div>
                        <Link href="/">
                            <a href="/"><img src="/images/logo.png" alt="LaunchAfrica logo" className="app-header__logo"/></a>
                        </Link>
                    </div>
                    <div className="app-header__content-right">
                        <Menu />
                    </div>
                </div>
            </div>
        );
    }
}

export default AppHeader;
