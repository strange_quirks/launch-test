import React, { Component } from 'react';
import Form from 'antd/lib/form';
import dynamic from "next/dynamic";

class DescriptionFormField extends Component {
    render() {
        const EditorNoSSR = dynamic(() => import("src/components/common/Editor"), { ssr: false });

        return (
            <Form.Item
                name="description"
                label={null}
                rules={[{required: true}]}
            >
                <EditorNoSSR onChange={this.props.onChange} data={this.props.data} />
            </Form.Item>
        );
    }
}

export default DescriptionFormField;
