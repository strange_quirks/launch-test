import React, { useState } from 'react';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Select from 'antd/lib/select';
import { isEmail } from 'src/helpers/helpers';

const ApplicationMethodFormField = ({ form }) => {
    let type = 'url';
    const applicationMethod = form.getFieldValue('applicationMethod')
    if (applicationMethod && isEmail(applicationMethod)) {
        type = 'email';
    }

    const [value, setValue] = useState(type);

    const select = (
        <Select defaultValue={type} onChange={(value) => setValue(value)}>
            <Select.Option value="url">Website</Select.Option>
            <Select.Option value="email">Email</Select.Option>
        </Select>
    );

    return (
        <Form.Item
            name="applicationMethod"
            label={<strong>Application method</strong>}
            rules={[{required: true, type: value}]}
            required={true}
            extra="Provide a website or email address where the applicant can apply for this job. Note: The email address will be public."
        >
            <Input addonBefore={select} placeholder="Enter a link to the application page or an email address"/>
        </Form.Item>
    );
};

export default ApplicationMethodFormField;
