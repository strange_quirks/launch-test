import React from 'react';
import Typography from 'antd/lib/typography';
import styles from 'src/components/styles/CategoryHeader.module.css';
import {REMOTE} from "src/helpers/africanCountries";

const CountryHeader = ({ country }) => {
    let headline = (
        <div>
            <Typography.Title level={2}>Startup and tech jobs in</Typography.Title>
            <Typography.Title className={styles.categoryHeader__headline}>
                <div className={styles.categoryHeader__category}>{country}</div>
            </Typography.Title>
        </div>
    );
    if (country === REMOTE) {
        headline = (
            <div>

                <Typography.Title className={styles.categoryHeader__headline}>
                    <div className={styles.categoryHeader__category}>Remote</div>
                </Typography.Title>
                <Typography.Title level={2}>Startup and tech jobs</Typography.Title>
            </div>
        );
    }

    return (
        <div className={styles.categoryHeader}>
            {headline}
            <div>
                <div className={styles.categoryHeader__image}>
                    <picture>
                        <source srcSet="/images/africa.webp" type="image/webp"/>
                        <source srcSet="/images/africa.png" type="image/png"/>
                        <img src="/images/africa.webp" type="image/webp" alt="Startup jobs in Africa" className={styles.categoryHeader__imageImg}/>
                    </picture>
                </div>
            </div>
        </div>
    );
};

export default CountryHeader;
