import { Component } from 'react';
import AutoComplete from 'antd/lib/auto-complete';
import Form from 'antd/lib/form';
import { getRootUrl } from "src/helpers/helpers";

class CompanySelect extends Component {
    state = {
        companies: [],
    };

    async componentDidMount() {
        const responseJobs = await fetch(`${getRootUrl()}/api/companies`);
        const companiesData = await responseJobs.json();

        const companies = [];
        companiesData.forEach(company => {
            companies.push({
                value: company.name,
                label: company.name,
                name: company.name,
                description: company.description,
                website: company.website,
                logo: company.logo,
            });
        });

        this.setState({
            companies,
        });
    }

    handleChange = (value, option) => {
        this.props.onChange(option);
    };

    render() {
        const { companies } = this.state;
        return (
            <Form.Item
                name="companyName"
                label={<strong>Company name</strong>}
                rules={[{required: true, min: 2, max: 150}]}
                required={true}
            >
                <AutoComplete
                    defaultOpen={false}
                    options={companies}
                    onChange={this.handleChange}
                    placeholder="Enter the name of the hiring company"
                    filterOption={(inputValue, option) => option.name.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                />
            </Form.Item>
        );
    }
}

export default CompanySelect;
