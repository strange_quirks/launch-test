import React from 'react';
import Select from 'antd/lib/select';
import Form from 'antd/lib/form';
import { ROLES } from 'src/helpers/roles';

const RoleSelectFormItem = () => {
    const roles = [];
    ROLES.forEach((role) => {
        roles.push(<Select.Option value={role} key={role}>{role}</Select.Option>);
    });

    return (
        <Form.Item name="role" label={<strong>Role</strong>} rules={[{required: true}]}
                   style={{display: 'inline-block', width: 'calc(50% - 10px)'}}>
            <Select
                placeholder="Select a role"
            >
                {roles}
            </Select>
        </Form.Item>
    );
};

export default RoleSelectFormItem;
