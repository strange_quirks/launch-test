import React from 'react';
import { InlineShareButtons } from "sharethis-reactjs";
import styles from 'src/components/styles/SocialShare.module.css';
import Typography from "antd/lib/typography";

const SocialShare = () => (
    <div className={styles.socialShare}>
        <div className={styles.socialShare__header}>
            <Typography.Title level={5}>Share this job</Typography.Title>
        </div>
        <InlineShareButtons
            config={{
                alignment: 'left',
                color: 'white',
                enabled: true,
                font_size: 16,
                labels: null,
                language: 'en',
                networks: [
                    'email',
                    'whatsapp',
                    'messenger',
                    'twitter',
                    'facebook',
                    'linkedin',
                ],
                padding: 10,
                radius: 1,
                show_total: false,
                size: 30,
            }}
        />
    </div>
);

export default SocialShare;
