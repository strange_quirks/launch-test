import React from 'react';
import Head from "next/head";
import { getCountryUrl, getRootUrl } from 'src/helpers/helpers';

const CountryHead = ({ country }) => {
    const keywords = ['LaunchAfrica, African Startup Job, African Tech Job, Job in Africa, tech jobs, remote jobs'];
    keywords.push(country);
    keywords.push(`${country} jobs`);
    keywords.push(`${country} tech jobs`);
    keywords.push(`${country} startup jobs`);
    keywords.push(`${country} remote jobs`);

    const title = `${country} Startup and Tech Jobs`;
    const description = `Find a startup or tech job in ${country}.`
    const url = `${getRootUrl()}${getCountryUrl(country)}`;

    return (
        <Head>
            <title>{title}</title>
            <meta name="keywords" content={keywords} />
            <meta name="description" content={description} key="description" />
            <meta name="robots" content="index, follow"/>
            <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
            <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
            <link rel="canonical" href={url} />
            <meta property="og:type" content="article" />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:description" content={description} key="ogDescription" />
            <meta property="og:url" content={url} key="ogUrl" />
            <meta property="og:site_name" content="LaunchAfrica" />
            <meta name="twitter:card" content="summary_large_image" />
        </Head>
    );
};

export default CountryHead;
