import React from 'react';
import Head from "next/head";
import { getCategoryUrl, getRootUrl } from 'src/helpers/helpers';

const CategoryHead = ({ category }) => {
    const keywords = ['LaunchAfrica, African Startup Job, African Tech Job, Job in Africa, tech jobs, remote jobs'];
    keywords.push(category);
    keywords.push(`${category} jobs in Africa`);
    keywords.push(`${category} jobs`);
    keywords.push(`${category} remote jobs`);
    keywords.push(`${category} tech jobs`);
    keywords.push(`${category} startup jobs`);

    const title = `${category} Startup and Tech Jobs`;
    const description = `Find a startup or tech job in ${category} in Africa.`
    const url = `${getRootUrl()}${getCategoryUrl(category)}`;

    return (
        <Head>
            <title>{title}</title>
            <meta name="keywords" content={keywords} />
            <meta name="description" content={description} key="description" />
            <link rel="canonical" href={url} />
            <meta property="og:type" content="article" />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:description" content={description} key="ogDescription" />
            <meta property="og:url" content={url} key="ogUrl" />
            <meta property="og:site_name" content="LaunchAfrica" />
            <meta name="twitter:card" content="summary_large_image" />
        </Head>
    );
};

export default CategoryHead;
