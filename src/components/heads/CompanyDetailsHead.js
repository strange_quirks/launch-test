import React from 'react';
import Head from "next/head";
import { getCompanyUrlByKey, getRootUrl } from 'src/helpers/helpers';

const CompanyDetailsHead = ({ company }) => {
    const keywords = ['LaunchAfrica, African Startup Job, African Tech Job, startup, Job in Africa, tech jobs, remote jobs'];
    keywords.push(company.name);
    keywords.push(`${company.name} jobs`);
    keywords.push(`${company.name} remote jobs`);
    keywords.push(`${company.name} tech jobs`);
    keywords.push(`${company.name} startup jobs`);
    keywords.push(`${company.name} jobs in Africa`);

    const title = `${company.name} Startup and Tech Jobs`;
    const description = `Find a Startup or tech job at ${company.name} in Africa.`;

    return (
        <Head>
            <title>{title}</title>
            <meta name="keywords" content={keywords} />

            <meta name="title" content={title} />
            <meta name="description" content={`${description} ${company.description}`} key="description" />

            <meta property="og:type" content="website" />
            <meta property="og:url" content={`${getRootUrl()}${getCompanyUrlByKey(company.key)}`} key="ogUrl" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:description" content={description} key="ogDescription" />
            <meta property="og:site_name" content="LaunchAfrica" />
            {company.logo.imageUrl && <meta property="og:image" content={company.logo.imageUrl} />}

            <meta property="twitter:card" content="summary_large_image" />
            <meta property="twitter:url" content={`${getRootUrl()}${getCompanyUrlByKey(company.key)}`} />
            <meta property="twitter:title" content={title} />
            <meta property="twitter:description" content={description} />
            {company.logo.imageUrl && <meta property="twitter:image" content={company.logo.imageUrl} />}
        </Head>
    );
};

export default CompanyDetailsHead;
