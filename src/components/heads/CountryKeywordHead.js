import React from 'react';
import Head from "next/head";
import { getCountryKeywordUrl, getRootUrl } from 'src/helpers/helpers';

const CountryKeywordHead = ({ country, keyword }) => {
    const keywords = ['LaunchAfrica, African Startup Job, African Tech Job, Job in Africa, tech jobs, remote jobs'];
    keywords.push(keyword);
    keywords.push(`${keyword} jobs in ${country}`);
    keywords.push(`${keyword} remote jobs in ${country}`);
    keywords.push(`${keyword} startup jobs in ${country}`);
    keywords.push(`${keyword} remote jobs`);
    keywords.push(`${keyword} startup jobs`);
    keywords.push(country);
    keywords.push(`${country} jobs`);
    keywords.push(`${country} startup jobs`);
    keywords.push(`${country} tech jobs`);
    keywords.push(`${country} remote jobs`);

    const title = `${keyword} Startup and Tech Jobs in ${country}`;
    const description = `Find the best ${keyword} startup and tech jobs in ${country}.`
    const url = `${getRootUrl()}${getCountryKeywordUrl(country, keyword)}`;

    return (
        <Head>
            <title>{title}</title>
            <meta name="keywords" content={keywords} />
            <meta name="description" content={description} key="description" />
            <link rel="canonical" href={url} />
            <meta property="og:type" content="article" />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:description" content={description} key="ogDescription" />
            <meta property="og:url" content={url} key="ogUrl" />
            <meta property="og:site_name" content="LaunchAfrica" />
            <meta name="twitter:card" content="summary_large_image" />
        </Head>
    );
};

export default CountryKeywordHead;
