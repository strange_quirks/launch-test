import React from 'react';
import Head from "next/head";
import { getKeywordUrl, getRootUrl } from 'src/helpers/helpers';

const KeywordHead = ({ keyword }) => {
    const keywords = ['LaunchAfrica, African Startup Job, African Tech Job, Job in Africa, tech jobs, remote jobs', 'startup jobs'];
    keywords.push(keyword);
    keywords.push(`${keyword} jobs`);
    keywords.push(`${keyword} hiring`);
    keywords.push(`${keyword} remote jobs`);

    const title = `${keyword} Startup and Tech Jobs`;
    const description = `Find a startup or tech job in ${keyword} in Africa or remotely.`
    const url = `${getRootUrl()}${getKeywordUrl(keyword)}`;

    return (
        <Head>
            <title>{title}</title>
            <meta name="keywords" content={keywords} />
            <link rel="canonical" href={url} />
            <meta name="description" content={description} key="description" />
            <meta property="og:type" content="article" />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:description" content={description} key="ogDescription" />
            <meta property="og:url" content={url} key="ogUrl" />
            <meta property="og:site_name" content="LaunchAfrica" />
            <meta name="twitter:card" content="summary_large_image" />
        </Head>
    );
};

export default KeywordHead;
