import React from 'react';
import Head from "next/head";
import { getRootUrl } from 'src/helpers/helpers';

const AboutHead = () => {
    const keywords = ['LaunchAfrica, African Startup Jobs, African Tech Jobs, Jobs in Africa, tech jobs, remote jobs'];
    const title = "Startup and Tech Jobs in Africa";
    const description = `LaunchAfrica is one of the largest job boards that focuses on the startup and tech scene in Africa. Find your perfect job among hundreds of job vacancies in Africa (or remote) for software engineers and developers, designers, product managers, marketing and sales experts, financial experts, lawyers, human resources and many more positions.`;

    return (
        <Head>
            <title>{title}</title>
            <meta name="keywords" content={keywords} />
            <meta name="description" content={description} key="description" />
            <meta name="robots" content="index, follow"/>
            <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
            <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
            <link rel="canonical" href={getRootUrl()} />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:description" content={description} key="ogDescription" />
            <meta property="og:url" content={getRootUrl()} key="ogUrl" />
            <meta property="og:site_name" content="LaunchAfrica" />
            <meta name="twitter:card" content="summary_large_image" />
        </Head>
    );
};

export default AboutHead;
