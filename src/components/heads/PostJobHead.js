import React from 'react';
import Head from "next/head";
import { getPostJobUrl, getRootUrl } from 'src/helpers/helpers';

const IndexHead = () => {
    const title = "Post a Job Vacancy";
    const description = `Find the best qualified candidates for your African startup or tech company.`
    const url = `${getRootUrl()}${getPostJobUrl()}`

    return (
        <Head>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />

            <title>{title}</title>
            <meta name="description" content={description} key="description" />
            <meta name="robots" content="index, follow"/>
            <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
            <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
            <link rel="canonical" href={url} />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:description" content={description} key="ogDescription" />
            <meta property="og:url" content={url} key="ogUrl"/>
            <meta property="og:site_name" content="LaunchAfrica" />
            <meta name="twitter:card" content="summary_large_image" />
        </Head>
    );
};

export default IndexHead;
