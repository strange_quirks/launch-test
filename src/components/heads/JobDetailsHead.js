import React from 'react';
import Head from "next/head";
import { getJobUrl, getRootUrl } from 'src/helpers/helpers';

const JobDetailsHead = ({ job }) => {
    const keywords = ['LaunchAfrica, African Startup Job, African Tech Job, Job in Africa, Career, tech jobs, remote jobs'];
    const skills = [];
    if (job.keywords && job.keywords.length >= 0) {
        job.keywords.forEach((keyword) => {
            keywords.push(keyword);
            skills.push(keyword);
        })
    }
    keywords.push(job.country);
    if (job.city) {
        keywords.push(job.city);
    }
    keywords.push(job.role);
    if (job.employmentType) {
        keywords.push(job.employmentType);
    }

    const title = `${job.title} | ${job.role} | ${job.companyName}`;
    const description = `Job vacancy as ${job.title} for ${job.companyName} in ${job.country}. Skills: ${skills.join(", ")}`

    return (
        <Head>
            <title>{title}</title>
            <meta name="keywords" content={keywords} />

            <meta name="title" content={title} />
            <meta name="description" content={description} key="description" />

            <meta property="og:type" content="website" />
            <meta property="og:url" content={`${getRootUrl()}${getJobUrl(job.title, job.id)}`} key="ogUrl" />
            <meta property="og:title" content={title} key="ogTitle" />
            <meta property="og:site_name" content="LaunchAfrica" />
            <meta property="og:description" content={description} key="ogDescription" />
            {job.companyLogo.imageUrl && <meta property="og:image" content={job.companyLogo.imageUrl} />}

            <meta property="twitter:card" content="summary_large_image" />
            <meta property="twitter:url" content={`${getRootUrl()}${getJobUrl(job.title, job.id)}`} />
            <meta property="twitter:title" content={title} />
            {job.companyLogo.imageUrl && <meta property="twitter:image" content={job.companyLogo.imageUrl} />}
        </Head>
    );
};

export default JobDetailsHead;
