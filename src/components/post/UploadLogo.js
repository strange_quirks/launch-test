import React, { Component } from "react";
import { storage } from "lib/firebase";
import Upload from 'antd/lib/upload';
import message from 'antd/lib/message';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import Resizer from 'react-image-file-resizer';

class UploadLogo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null,
            imageUrl: props.imageUrl,
        };
    }

    handleChange = e => {
        if (e.file) {
            const image = e.file;
            this.setState(() => ({ image }));
        }
    };

    handleUpload = async (e) => {
        let image = e.file;
        if (!image) {
            message.error('Image not found');
        }

        const resizeFile = (image) => new Promise(resolve => {
            Resizer.imageFileResizer(image, 100, 100, 'WEBP', 100, 0,
                uri => {
                    resolve(uri);
                },
                'blob'
            );
        });

        const resizedImage = await resizeFile(image);

        const imageName = `${Date.now()}_${image.name}`;

        const uploadTask = storage.ref(`images/${imageName}`).put(resizedImage);
        uploadTask.on(
            "state_changed",
            snapshot => {
                const progress = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
                this.setState({ progress });
            },
            (error) => {
                message.error(`Image upload error: ${error}`);
            },
            () => {
                storage
                    .ref("images")
                    .child(imageName)
                    .getDownloadURL()
                    .then((imageUrl) => {
                        this.props.onImageUploaded({ imageUrl, imageName });
                        this.setState({ imageUrl, imageName });
                    });
            }
        );
    };

    beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG files');
        }

        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must be smaller than 2MB');
        }

        return isJpgOrPng && isLt2M;
    }

    render() {
        const uploadButton = (
            <div>
                {this.state.loading ? <LoadingOutlined /> : <PlusOutlined />}
                <div className="ant-upload-text">Upload image</div>
            </div>
        );
        const imageUrl = this.state.imageUrl || this.props.form.getFieldValue('companyLogo')?.imageUrl;

        return (
            <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                customRequest={this.handleUpload}
                beforeUpload={this.beforeUpload}
                onChange={this.handleChange}
            >
                {imageUrl ? <img src={imageUrl} alt="Avatar" style={{ width: '100%' }} /> : uploadButton}
            </Upload>
        );
    }
}

export default UploadLogo;
