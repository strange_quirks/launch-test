import React from 'react';
import Typography from 'antd/lib/typography';
import styles from 'src/components/styles/PostJob.module.css';

const PostHeaderEdit = ({ status }) => (
    <div className={styles.postJobHeader}>
        <Typography.Title>Edit your job post</Typography.Title>
        <Typography.Title level={4}>Status: {status}</Typography.Title>
    </div>
);

export default PostHeaderEdit;
