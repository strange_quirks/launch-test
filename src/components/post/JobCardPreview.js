import { Component } from 'react';
import JobCardHeader from "src/components/jobs/jobCard/elements/JobCardHeader";
import JobCardMeta from "src/components/jobs/jobCard/elements/JobCardMeta";
import JobCardKeywords from "src/components/jobs/jobCard/elements/JobCardKeywords";
import Typography from 'antd/lib/typography';
import styles from 'src/components/styles/PostJob.module.css';

class JobCardPreview extends Component {
    render() {
        let { form } = this.props;

        const job = {
            title: form.getFieldValue('title'),
            companyName: form.getFieldValue('companyName'),
            companyLogo: form.getFieldValue('companyLogo'),
            city: form.getFieldValue('city'),
            country: form.getFieldValue('country'),
            employmentType: form.getFieldValue('employmentType'),
            role: form.getFieldValue('role'),
            keywords: form.getFieldValue('keywords'),
        }

        return (
            <div className={styles.jobCardPreview}>
                <Typography.Title level={2}>Preview</Typography.Title>
                <div className={styles.jobCard}>
                    <JobCardHeader job={job} />
                    <JobCardMeta job={job} />
                    <JobCardKeywords job={job} />
                </div>
            </div>
        );
    }
}

export default JobCardPreview;
