import React, { useState } from 'react';
import Typography from 'antd/lib/typography';
import Button from 'antd/lib/button';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Checkbox from 'antd/lib/checkbox';
import Tooltip from 'antd/lib/tooltip';
import UploadLogo from "./UploadLogo";
import CountrySelectFormItem from 'src/components/common/CountrySelectFormItem';
import { SendOutlined } from '@ant-design/icons';
import DescriptionFormField from "src/components/common/DescriptionFormField";
import KeywordSelectFormItem from "src/components/common/KeywordSelectFormItem";
import RoleSelectFormItem from "src/components/common/RoleSelectFormItem";
import EmploymentTypeSelectFormItem from "src/components/common/EmploymentTypeSelectFormItem";
import { STATUS_PENDING } from 'src/helpers/constants';
import ApplicationMethodFormField from "src/components/common/ApplicationMethodFormField";
import { encrypt, decrypt } from 'src/helpers/helpers';
import CompanySelect from "src/components/common/CompanySelect";
import styles from 'src/components/styles/PostJob.module.css';
import SalaryFormItem from "src/components/common/SalaryFormItem";
import { REMOTE } from "src/helpers/africanCountries";

export default function PostJobForm({ job, onSave, onAfterSave, additionalActions }) {
    const [form] = Form.useForm();

    const [isSubmitting, setIsSubmitting] = useState(false);

    const submitJobPost = async(data) => {
        setIsSubmitting(true);

        const currentDate = new Date();
        let endDate = new Date();
        endDate.setDate(endDate.getDate() + 60);
        const formData = {
            title: data.title.replace('/', '-').trim(), // "/" should not be in the url.
            keywords: data.keywords,
            keywordsLowercase: data.keywords.map((keyword) => keyword.toLowerCase()),
            role: data.role,
            country: data.country || null,
            countryList: data.countryList,
            city: data.country && data.city ? data.city : null,
            employmentType: data.employmentType || null,
            description: JSON.stringify(data.description),
            applicationMethod: data.applicationMethod,
            applicationDetails: data.applicationDetails || null,
            companyEmail: encrypt(data.companyEmail, false),
            companyName: data.companyName.trim(),
            companyWebsite: data.companyWebsite.trim(),
            baseSalary: data.baseSalary || null,
            currency: data.currency || null,
            companyDescription: data.companyDescription || null,
            companyLogo: data.companyLogo,
            createdAt: job?.createdAt || currentDate,
            startTimestamp: job?.startTimestamp || Date.now(),
            updatedAt: currentDate,
            endDate: job?.endDate || endDate,
            status: job?.status || STATUS_PENDING,
        };

        const res = await onSave(formData);
        if (res && res.id) {
            setIsSubmitting(false);
            if (onAfterSave) {
                onAfterSave(res.id);
                form.resetFields();
            }
        } else {
            setIsSubmitting(false);
        }
    }

    const handleSubmitForm = (data) => {
        submitJobPost(data);
    };

    const validateMessages = {
        required: "This field is required.",
        string: {
            min: "This field must be at least ${min} characters.",
            max: "This field cannot be longer than ${max} characters.",
            range: "This field must be between ${min} and ${max} characters.",
        },
        types: {
            url: "This field is not a valid url. Valid example: https://www.example.com",
            email: "This field is not a valid email. Valid example: hello@example.com"
        }
    };

    let jobDescription = {};
    let initialValues = {
        currency: "USD",
    };
    if (job) {
        initialValues = {...job};
        jobDescription = JSON.parse(initialValues.description);
        initialValues.description = jobDescription;
        initialValues.companyEmail = decrypt(job.companyEmail, false);
        initialValues.currency = job.currency || "USD";
        initialValues.remote = job.countryList && job.countryList.includes(REMOTE);
    }

    return (
        <div className={styles.postJobForm}>
            <Form
                form={form}
                layout="vertical"
                size="large"
                onFinish={handleSubmitForm}
                scrollToFirstError={true}
                validateMessages={validateMessages}
                initialValues={initialValues}
            >
                <div className={styles.postJobForm__container}>
                    <div>
                        <div className={styles.postJobForm__card}>
                            <Typography.Title level={3} className={styles.postJobForm__cardHeader}>Tell us about the job</Typography.Title>
                            <Form.Item
                                name="title"
                                label={<strong>Job post title</strong>}
                                rules={[{required: true, min: 5, max: 115}]}
                                required={true}
                                extra="The title must describe one position only and should accurately describe the job."
                            >
                                <Input placeholder="Enter a title for your job post e.g. Senior Frontend Software Engineer" autoFocus={true}/>
                            </Form.Item>
                            <KeywordSelectFormItem form={form} />
                            <Form.Item>
                                <RoleSelectFormItem />
                                <span
                                    style={{display: 'inline-block', width: '20px', lineHeight: '32px', textAlign: 'center'}}
                                />
                                <div style={{display: 'inline-block', width: 'calc(50% - 10px)'}}>
                                    <EmploymentTypeSelectFormItem />
                                </div>
                            </Form.Item>
                            <Form.Item>
                                <SalaryFormItem
                                    currency={initialValues.currency}
                                    onCurrencySelect={(currency) =>
                                        form.setFieldsValue({
                                            currency: currency,
                                        })
                                    }
                                />
                            </Form.Item>
                        </div>

                        <div className={styles.postJobForm__card}>
                            <Form.Item
                                name="countryList"
                                label={null}
                                rules={[{required: true}]}
                                help="Specify a location and/or whether the job is remote."
                            >
                                <Typography.Title level={3} className={styles.postJobForm__cardHeader}>
                                    Location
                                </Typography.Title>
                            </Form.Item>
                            <Typography>
                                <div style={{ marginBottom: '20px' }}>
                                    <div>You can specify a country and city where the job is located.</div>
                                    <div>
                                        Additionally, or as an alternative, select the checkbox if this is a{` `}
                                        <Tooltip title="A remote job is one that anyone from around the world can apply for. The job is location independent.">
                                            <a>remote job</a>
                                        </Tooltip>.
                                    </div>
                                </div>
                            </Typography>
                            <Form.Item>
                                <CountrySelectFormItem
                                    onChange={(value) => {
                                        const countryList = [];
                                        if (value) {
                                            countryList.push(value);
                                        }
                                        if (form.getFieldValue('remote')) {
                                            countryList.push(REMOTE);
                                        }

                                        form.setFieldsValue({
                                            countryList: countryList,
                                        });
                                    }}
                                />
                                <span
                                    style={{display: 'inline-block', width: '20px', lineHeight: '32px', textAlign: 'center'}}
                                />
                                <Form.Item
                                    name="city"
                                    label={<strong>City</strong>}
                                    required={false}
                                    style={{display: 'inline-block', width: 'calc(50% - 10px)'}}
                                >
                                    <Input placeholder="Enter the city name"/>
                                </Form.Item>
                            </Form.Item>
                            <Form.Item label={<strong>Is this a remote job?</strong>}>
                                <Form.Item name="remote" valuePropName="checked" className={styles.postJobForm__remoteFormItem}>
                                    <Checkbox onChange={(event) => {
                                        const countryList = [];
                                        if (form.getFieldValue('country')) {
                                            countryList.push(form.getFieldValue('country'));
                                        }
                                        if (event.target.checked === true) {
                                            countryList.push(REMOTE);
                                        }

                                        form.setFieldsValue({
                                            countryList: countryList,
                                        });
                                    }}>Anyone can apply. Candidates can work fully remotely.</Checkbox>
                                </Form.Item>
                            </Form.Item>
                        </div>

                        <div className={styles.postJobForm__card}>
                            <Typography.Title level={3} className={styles.postJobForm__cardHeader}>Job description</Typography.Title>
                            <DescriptionFormField
                                data={jobDescription}
                                onChange={(data) => {
                                    form.setFieldsValue({
                                        description: data,
                                    })
                                }}
                            />
                        </div>

                        <div className={styles.postJobForm__card}>
                            <Typography.Title level={3} className={styles.postJobForm__cardHeader}>How do applicants apply</Typography.Title>
                            <ApplicationMethodFormField form={form} />
                            <Form.Item name="applicationDetails" label={<strong>Application details</strong>} required={false}>
                                <Input.TextArea placeholder="Enter any information the applicant should know on how to apply (optional)" rows={5}/>
                            </Form.Item>
                        </div>

                        <div className={styles.postJobForm__card}>
                            <Typography.Title level={3} className={styles.postJobForm__cardHeader}>Tell us about the company</Typography.Title>
                            <Form.Item
                                name="companyEmail"
                                label={<strong>Company email (private)</strong>}
                                rules={[{required: true, type: 'email'}]}
                                required={true}
                                extra="IMPORTANT: This will be kept private. Ensure it is correct as it will be used for any communication with us and can be used for editing of the job."
                            >
                                <Input placeholder="Enter your email address"/>
                            </Form.Item>
                            <Form.Item>
                                <div style={{display: 'inline-block', width: 'calc(75% - 10px)'}}>
                                    <CompanySelect
                                        onChange={(company) => {
                                            const fields = {};
                                            if (!form.getFieldValue('companyLogo')) {
                                                fields.companyLogo = company?.logo;
                                            }
                                            if (!form.getFieldValue('companyDescription')) {
                                                fields.companyDescription = company?.description;
                                            }
                                            if (!form.getFieldValue('companyWebsite')) {
                                                fields.companyWebsite = company?.website;
                                            }
                                            form.setFieldsValue(fields);
                                        }}
                                    />
                                    <Form.Item
                                        name="companyWebsite"
                                        label={<strong>Company website</strong>}
                                        rules={[{required: true, type: 'url'}]}
                                        required={true}
                                    >
                                        <Input placeholder="Enter the URL of the company website"/>
                                    </Form.Item>
                                </div>
                                <span
                                    style={{display: 'inline-block', width: '20px', lineHeight: '32px', textAlign: 'center'}}
                                />
                                <Form.Item
                                    name="companyLogo"
                                    label={<strong>Company logo</strong>}
                                    rules={[{required: true}]}
                                    required={true} style={{display: 'inline-block', width: 'calc(25% - 10px)'}}
                                >
                                    <UploadLogo
                                        form={form}
                                        imageUrl={job?.companyLogo?.imageUrl}
                                        onImageUploaded={(image) => {
                                            form.setFieldsValue({
                                                companyLogo: image,
                                            })
                                        }}
                                    />
                                </Form.Item>
                            </Form.Item>
                            <Form.Item name="companyDescription" label={<strong>Company description</strong>} required={false}>
                                <Input.TextArea placeholder="Enter any information describing the company (optional)" rows={10}/>
                            </Form.Item>
                        </div>

                        <div className={styles.postJobForm__card}>
                            <Button type="primary" htmlType="submit" size="large" block={true} loading={isSubmitting}>
                                <SendOutlined/> <strong>{job ? 'Update job post' : 'Post job for free'}</strong>
                            </Button>
                        </div>
                    </div>
                    <div className={styles.postJobForm__right}>
                        <div className={styles.postJobForm__card}>
                            <Button type="primary" htmlType="submit" size="large" block={true} loading={isSubmitting}>
                                <SendOutlined/> <strong>{job ? 'Update job post' : 'Post job for free'}</strong>
                            </Button>
                        </div>
                        {additionalActions && <div className={styles.postJobForm__cardAdditionalActions}>
                            {additionalActions}
                        </div>}
                    </div>
                </div>
            </Form>
        </div>
    );
}
