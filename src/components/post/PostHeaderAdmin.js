import React from 'react';
import Typography from 'antd/lib/typography';
import styles from 'src/components/styles/PostJob.module.css';

const PostHeaderAdmin = ({ job }) => (
    <div className={styles.postJobHeader}>
        <Typography.Title>Edit job post</Typography.Title>
        <Typography.Title level={2}>{job.title} - <i>{job.status}</i></Typography.Title>
    </div>
);

export default PostHeaderAdmin;
