import React from 'react';
import Typography from 'antd/lib/typography';
import styles from 'src/components/styles/PostJob.module.css';

const PostHeader = () => (
    <div className={styles.postJobHeader}>
        <div className={styles.postJobHeader__content}>
            <div>
                <Typography.Title>Post a job vacancy</Typography.Title>
                <Typography.Title level={4}>Post a job for free to find qualified candidates in Africa and all over the world.</Typography.Title>
            </div>
            <div className={styles.postJobHeader__contentRight}>
                <Typography>Alternatively, you can send us your job post via email and we will post it online for you: support@launchafrica.io</Typography>
                <Typography>This is only for a limited time!</Typography>
            </div>
        </div>
    </div>
);

export default PostHeader;
