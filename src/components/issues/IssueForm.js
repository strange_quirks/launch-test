import React, { useState } from 'react';
import Button from 'antd/lib/button';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import message from 'antd/lib/message';
import { FlagOutlined } from "@ant-design/icons";

export default function IssueForm({ jobId, onSubmitted }) {
    const [form] = Form.useForm();

    const [isSubmitting, setIsSubmitting] = useState(false);

    const addIssue = async(description)  => {
        setIsSubmitting(true);

        const data = {
            jobId: jobId,
            description,
            createdAt: new Date()
        };

        const url = `/api/issues/create`
        const result = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const res = await result.json();
        if (res && res.id) {
            setIsSubmitting(false);
            message.success('Your issue was submitted');
            form.resetFields();
            onSubmitted();
        } else {
            setIsSubmitting(false);
            message.error('Something went wrong. Please try again later.');
        }
    }

    const handleSubmitForm = ({ description }) => {
        addIssue(description);
    };

    return (
        <Form
            form={form}
            name="IssueForm"
            size="large"
            onFinish={handleSubmitForm}
            preserve={false}
        >
            <Form.Item
                name="description"
                rules={[
                    {
                        required: true,
                        message: 'Enter the details',
                    },
                ]}
            >
                <Input.TextArea
                    placeholder="For any issues with this job post, please enter the details here and we will look into it."
                    autoSize={{ minRows: 4, maxRows: 10 }}
                    style={{ fontSize: '16px' }}
                />
            </Form.Item>

            <Form.Item>
                <Button size="default" type="primary" htmlType="submit" block={true} loading={isSubmitting}>
                    <FlagOutlined /> Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
