import React from 'react';
import Modal from 'antd/lib/modal';
import Typography from 'antd/lib/typography';
import * as PropTypes from "prop-types";
import IssueForm from "./IssueForm";

const IssueModal = ({ jobId, isOpen, onClose }) => {
    return (
        <Modal
            title={
                <Typography.Title level={3}>
                    Report job post
                </Typography.Title>
            }
            centered={true}
            visible={isOpen}
            onCancel={onClose}
            maskClosable={true}
            footer={null}
        >
            <IssueForm jobId={jobId} onSubmitted={onClose}/>
        </Modal>
    );
};

IssueModal.propTypes = {
    jobId: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired
}

export default IssueModal;
