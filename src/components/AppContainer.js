import React, { Component } from 'react';
import Button from 'antd/lib/button';
import CloseOutlined from '@ant-design/icons/CloseOutlined';
import QuestionOutlined from '@ant-design/icons/QuestionOutlined';
import TwitterOutlined from '@ant-design/icons/TwitterOutlined';
import QuestionCircleOutlined from '@ant-design/icons/QuestionCircleOutlined';
import {
    isMobile,
} from "react-device-detect";
import ReactGA from 'react-ga';
import Head from "next/head";
import { getRootUrl } from 'src/helpers/helpers';
import Link from 'next/link';

if (process.env.NODE_ENV !== 'development') {
    ReactGA.initialize('UA-181130558-1');
}

class AppContainer extends Component {
    state = {
        showFeedback: false,
    };

    componentDidMount() {
        if (process.env.NODE_ENV !== 'development') {
            ReactGA.pageview(window.location.pathname)
        }
    }

    render() {
        const title = "LaunchAfrica | African Startup & Tech Jobs";
        return (
            <>
                <Head>
                    <meta httpEquiv="content-language" content="en" />
                    <meta name="theme-color" content="#ffffff" />
                    <meta charSet="UTF-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <title>{title}</title>
                    <link rel="icon" href={`${getRootUrl()}/favicons/favicon.ico`} type="image/x-icon" />
                    <link rel="apple-touch-icon" sizes="76x76" href={`${getRootUrl()}/favicons/apple-touch-icon.png`} />
                    <link rel="icon" type="image/png" sizes="32x32" href={`${getRootUrl()}/favicons/favicon-32x32.png`} />
                    <link rel="icon" type="image/png" sizes="16x16" href={`${getRootUrl()}/favicons/favicon-16x16.png`} />
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
                    <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
                    <meta property="og:image" content={`${getRootUrl()}/images/socialcover.png`} key="ogImage"/>
                    <meta property="og:type" content="article" />
                    <meta property="og:description" content="Find jobs in startup & tech companies in Africa." key="ogDescription"/>
                    <meta property="og:locale" content="en_US" />
                    <meta property="og:type" content="website" />
                    <meta property="og:title" content={title} key="ogTitle" />
                    <meta property="og:url" content={getRootUrl()} key="ogUrl" />
                    <meta property="og:site_name" content="LaunchAfrica" />
                    <meta name="description" content="Find jobs in startup & tech companies in Africa." key="description" />
                </Head>

                <div className="app-container">
                    <div className="app-container__body">
                        <div className="app-container__content">
                            {this.props.children}
                        </div>
                    </div>
                </div>
                <div className="app-container__footer">
                    <div className="app-container__footer-info">
                        &copy; LaunchAfrica {(new Date()).getFullYear()} &middot; <Link href="/privacy" aria-label="Privacy">
                        <a href="/privacy">Privacy</a>
                    </Link> &middot; <Link href="/about" aria-label="About">
                        <a href="/about">About us</a>
                    </Link> &middot; <a href="https://twitter.com/africa_launch" target="_blank" rel="noopener"><TwitterOutlined /> Follow us on Twitter</a>
                    </div>
                    <div className="app-container__feedback">
                        {isMobile && <Button className="app-container__toggle-showing" type="primary" icon={this.state.showFeedback ? <CloseOutlined /> : <QuestionOutlined />} size="medium" onClick={() => this.setState({ showFeedback: !this.state.showFeedback })}/>}
                        {(this.state.showFeedback || !isMobile) && <>
                            <Button
                                className="app-container__feedback-button"
                                type="primary"
                                size="medium"
                                icon={<span><QuestionCircleOutlined /></span>}
                                href="https://qrfeedbackcreator.com/form/LguXLR6ko"
                                target="_blank"
                                rel="noopener"
                                aria-label="Feedback"
                            >&nbsp;Feedback</Button>
                        </>}
                    </div>
                </div>
                <script
                    type="text/javascript"
                    dangerouslySetInnerHTML={{ __html: `
                        var _smartsupp = _smartsupp || {};
_smartsupp.key = '42dd3103043da7aff7b5c15075a3ace3bf48a134';
window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
                    ` }}
                />
            </>
        );
    }
}

export default AppContainer;
