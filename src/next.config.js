const webpack = require('webpack');
const path = require('path');
require('dotenv').config();

// const withBundleAnalyzer = require('@next/bundle-analyzer')({
//     enabled: process.env.ANALYZE === 'true',
// })

module.exports = {
    distDir: '../.next',
    webpack: config => {
        const env = Object.keys(process.env).reduce((acc, curr) => {
            acc[`process.env.${curr}`] = JSON.stringify(process.env[curr]);
            return acc;
        }, {});

        config.resolve.modules.push(path.resolve('./'));
        config.plugins.push(new webpack.DefinePlugin(env));

        return config;
    },
    images: {
        domains: ['firebasestorage.googleapis.com'],
    },
};
