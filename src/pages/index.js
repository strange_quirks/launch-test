import { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import LandingHeader from "src/components/application/LandingHeader";
import JobCardList from "src/components/jobs/JobCardList";
import IndexHead from "src/components/heads/IndexHead";
import { QUERY_LIMIT } from 'src/helpers/constants';
import CompanySpotlight from "src/components/company/CompanySpotlight";
import styles from 'src/components/styles/Index.module.css';
import Categories from "src/components/common/Categories";
import FiltersContainer from "src/components/jobs/FiltersContainer";
import { getRootUrl } from "src/helpers/helpers";

class Index extends Component {
    state = {
        jobs: this.props.jobs,
        filters: {},
        hasMore: true,
    };

    getJobs = () => {
        if (!this.state.hasMore) {
            return;
        }

        this.firestoreGetJobs().then((data) => {
            this.setState(data)
        })
    }

    async firestoreGetJobs() {
        const filters = {};
        if (this.state.filters.role) {
            filters.role = this.state.filters.role;
        }
        if (this.state.filters.employmentType) {
            filters.employmentType = this.state.filters.employmentType;
        }
        if (this.state.filters.country) {
            filters.country = this.state.filters.country;
        }

        const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
            method: 'POST',
            body: JSON.stringify({
                limit: QUERY_LIMIT,
                filters,
                startAfter: this.state.jobs && this.state.jobs.length > 0 ? this.state.jobs[this.state.jobs.length - 1].startTimestamp : null
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const jobsData = await responseJobs.json();

        const jobs = this.state.jobs;
        jobsData.forEach(job => {
            jobs.push(job);
        });

        return {
            jobs,
            hasMore: jobsData.length > 0,
        };
    }

    handleUpdate = (filters) => {
        this.setState({ filters, jobs: [], hasMore: true }, () => {
            this.getJobs();
        });
    }

    render() {
        return (
            <AppContainer>
                <IndexHead />
                <LandingHeader />
                <div className={styles.indexContainer}>
                    <Categories />
                    <CompanySpotlight />
                    <FiltersContainer onUpdate={this.handleUpdate} />
                    <JobCardList
                        jobs={this.state.jobs}
                        onLoadMore={this.getJobs}
                    />
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps() {
    const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
        method: 'POST',
        body: JSON.stringify({
            limit: QUERY_LIMIT,
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const jobsData = await responseJobs.json();

    return {
        props: { jobs: jobsData }
    };
};

export default Index;
