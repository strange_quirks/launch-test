import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import JobCardList from "src/components/jobs/JobCardList";
import SearchHead from "src/components/heads/SearchHead";
import styles from "src/components/styles/Search.module.css";
import AppHeader from "src/components/common/AppHeader";
import Typography from "antd/lib/typography";
import Input from "antd/lib/input";
import { QUERY_LIMIT } from "src/helpers/constants";

const algoliasearch = require('algoliasearch');

class Search extends Component {
    state = {
        jobs: this.props.jobs,
        page: 1,
        hasMore: this.props.hasMore,
    };

    getJobs = () => {
        if (!this.state.hasMore) {
            return;
        }

        this.algoliaSearch().then((data) => {
            this.setState(data)
        })
    }


    async algoliaSearch() {
        const client = algoliasearch(process.env.ALGOLIA_APPLICATION_ID, process.env.ALGOLIA_SEARCH_KEY);
        const index = client.initIndex('prod_JOBS');

        const searchResults = await index
            .search(this.props.query, {
                page: this.state.page,
                hitsPerPage: QUERY_LIMIT
            })
            .then((responses) => {
                return responses.hits;
            });

        const jobs = this.state.jobs;
        searchResults.forEach(job => {
            jobs.push(Object.assign({ id: job.objectID }, job));
        });

        return {
            jobs,
            hasMore: searchResults && searchResults.length === QUERY_LIMIT,
            page: this.state.page + 1,
        }
    }

    render() {
        return (
            <AppContainer>
                <SearchHead query={this.props.query} />
                <AppHeader />
                <div className={styles.searchContainer}>
                    <div className={styles.searchContainer__header}>
                        <Typography.Title level={1} className={styles.searchContainer__headerTitle}>
                            <div>Showing search results for <span className={styles.searchContainer__headerQuery}>{this.props.query}</span></div>
                            <div className={styles.searchContainer__headerSearch}>
                                <Input.Search defaultValue={this.props.query} placeholder="Start searching. e.g. JavaScript, Marketing, Zambia, ..." size="large" onSearch={(value) => {
                                    if (value) {
                                        window.location.href = `/search/${value}`;
                                    }
                                }} enterButton={true} />
                            </div>
                        </Typography.Title>
                        <img src="/images/algolia.png" className={styles.searchContainer__algoliaLogo} alt="Algolia logo"/>
                    </div>
                    <JobCardList
                        jobs={this.state.jobs}
                        onLoadMore={this.getJobs}
                    />
                </div>
            </AppContainer>
        );
    }
}

Search.getInitialProps = async ({ query, res }) => {
    const search = query.search;
    if (!search) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    const client = algoliasearch(process.env.ALGOLIA_APPLICATION_ID, process.env.ALGOLIA_SEARCH_KEY);
    const index = client.initIndex('prod_JOBS');

    const searchResults = await index
        .search(search, {
            page: 0,
            hitsPerPage: QUERY_LIMIT
        })
        .then((responses) => {
            return responses.hits;
        });


    return {
        jobs: searchResults.map((job) => Object.assign({ id: job.objectID }, job)),
        hasMore: searchResults && searchResults.length === QUERY_LIMIT,
        query: search,
    }
};

export default Search;
