import React from 'react';
import App from 'next/app';
import 'antd/dist/antd.min.css';
import 'src/public/css/AppContainer.css';
import NProgress from 'nprogress';
import Router from 'next/router';

Router.onRouteChangeStart = () => {
    NProgress.start();
};

Router.onRouteChangeComplete = () => {
    NProgress.done();
    document.body.scrollTop = 0;
};

Router.onRouteChangeError = () => {
    NProgress.done();
};

class MyApp extends App {
    render() {
        const { Component, pageProps, router } = this.props;

        return (
            <Component {...pageProps} key={router.route} />
        );
    }
}

export default MyApp;
