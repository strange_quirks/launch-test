import { db } from "lib/firebase";
import { getSerializableCompany } from "src/helpers/serializable";

export default async function companyByKey(req, res) {
    const {
        query: { companyKey },
    } = req;

    if (!companyKey) {
        res.status(500).json({});
        res.end();
    }

    const companyRef = db.collection('companies').where('key', '==', companyKey);
    const firebaseQuery = await companyRef.get();
    if (!firebaseQuery || firebaseQuery.empty) {
        res.status(500).json({});
        res.end();
    }

    const doc = firebaseQuery.docs[0];
    res.status(200).json(getSerializableCompany(Object.assign({id: doc.id}, doc.data())));
    res.end();
}
