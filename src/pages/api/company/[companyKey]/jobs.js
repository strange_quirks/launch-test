import { db } from "lib/firebase";
import {getSerializableCompany, getSerializableJob} from "src/helpers/serializable";
import {QUERY_LIMIT, STATUS_ACTIVE} from "src/helpers/constants";

export default async function jobsByCompany(req, res) {
    const { name, limit, startAfter } = req.body;
    if (!name) {
        res.status(500).json({});
        res.end();
    }

    let jobsRef = db.collection("jobs").where('companyName', '==', name).where('status', '==', STATUS_ACTIVE).orderBy('startTimestamp', 'desc').limit(limit);
    if (startAfter) {
        jobsRef = jobsRef.startAfter(startAfter);
    }
    const snapshot = await jobsRef.get();
    const jobs = snapshot && snapshot.docs.map(d => {
        const data = Object.assign({id: d.id}, d.data());
        return getSerializableJob(data);
    });

    res.status(200).json(jobs);
    res.end();
}
