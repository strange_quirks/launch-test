import { db, firebaseApp } from "lib/firebase";

export default async function incrementViews(req, res) {
    const {
        query: { jobId },
    } = req;
    if (!jobId) {
        res.status(500).json({});
        res.end();
    }

    const increment = firebaseApp.firestore.FieldValue.increment(1);

    const ref = await db.collection('jobStats').doc(jobId);
    const doc = await ref.get();
    if (!doc.exists) {
        ref.set({ views: increment });
    } else {
        ref.update({ views: increment });
    }

    res.status(200).json({});
    res.end();
}
