import { db } from "lib/firebase";
import { getSerializableJob } from "src/helpers/serializable";

export default async function job(req, res) {
    const {
        query: { jobId },
    } = req;
    if (!jobId) {
        res.status(500).json({});
        res.end();
    }

    const ref = db.collection('jobs').doc(jobId);
    const doc = await ref.get();
    if (!doc.exists) {
        res.status(500).json({});
        res.end();
    }

    res.status(200).json(getSerializableJob(Object.assign({id: doc.id}, doc.data())));
    res.end();
}
