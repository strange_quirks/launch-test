import { db, firebaseApp } from "lib/firebase";

export default async function incrementApplies(req, res) {
    const {
        query: { jobId },
    } = req;
    if (!jobId) {
        res.status(500).json({});
        res.end();
    }

    const increment = firebaseApp.firestore.FieldValue.increment(1);

    const ref = await db.collection('jobStats').doc(jobId);
    const doc = await ref.get();
    if (!doc.exists) {
        ref.set({ applies: increment });
    } else {
        ref.update({ applies: increment });
    }

    res.status(200).json({});
    res.end();
}
