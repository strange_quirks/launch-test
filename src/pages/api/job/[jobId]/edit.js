import { db } from "lib/firebase";
import {decrypt} from "src/helpers/helpers";

export default function handler(req, res) {
    if (req.method === 'POST') {
        const {
            query: { jobId },
        } = req;

        const encryptedJobId = req.body.ejid;
        if (!encryptedJobId || jobId !== decrypt(encryptedJobId)) {
            return res.status(500).json({});
        }

        db.collection('jobs').doc(jobId).set(req.body).then(() => res.status(200).json({}));
    } else {
        res.status(500);
    }
}
