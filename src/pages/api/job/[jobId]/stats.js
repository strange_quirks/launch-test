import { db } from "lib/firebase";

export default async function stats(req, res) {
    const {
        query: { jobId },
    } = req;

    if (!jobId) {
        res.status(500).json({});
        res.end();
    }

    const ref = db.collection('jobStats').doc(jobId);
    const doc = await ref.get();
    if (!doc.exists) {
        res.status(500).json({});
        res.end();
    }

    res.status(200).json(doc.data());
    res.end();
}
