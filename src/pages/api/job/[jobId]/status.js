import { db } from "lib/firebase";
import { decrypt } from "src/helpers/helpers";

export default function status(req, res) {
    const {
        query: { jobId },
    } = req;

    const encryptedJobId = req.body.ejid;
    if (!encryptedJobId || jobId !== decrypt(encryptedJobId)) {
        res.status(500).json({});
        res.end();
    }

    db.collection('jobs').doc(jobId).update({
        status: req.body.status,
    }).then(() => res.status(200).json({}));
}
