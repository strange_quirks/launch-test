import { db } from "lib/firebase";
import { STATUS_ACTIVE } from "src/helpers/constants";
import { getSerializableJob } from "src/helpers/serializable";

export default async function similarJobs(req, res) {
    const {
        query: { jobId },
        body,
    } = req;

    let jobsRef = db.collection("jobs").where('role', '==', body.role).where('status', '==', STATUS_ACTIVE).orderBy('startTimestamp', 'desc').limit(100);

    const snapshot = await jobsRef.get();
    const jobs = snapshot && snapshot.docs.map(d => {
        const data = Object.assign({id: d.id}, d.data());
        return getSerializableJob(data);
    });

    res.status(200).json(jobs);
    res.end();
}
