import { db } from "lib/firebase";

export default async function createIssue(req, res) {
    if (req.method === 'POST') {
        const doc = await db.collection('issues').add(req.body);
        res.status(200).json({
            id: doc.id
        });
        res.end();
    } else {
        res.status(500);
        res.end();
    }
}
