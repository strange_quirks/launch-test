import { db } from "lib/firebase";
import { getSerializableJob} from "src/helpers/serializable";
import { STATUS_ACTIVE} from "src/helpers/constants";
import { REMOTE } from "src/helpers/africanCountries";

export default async function findJobs(req, res) {
    const { limit, startAfter, filters } = req.body;

    let jobsRef = db.collection("jobs").where('status', '==', STATUS_ACTIVE).orderBy('startTimestamp', 'desc').limit(limit);
    if (startAfter) {
        jobsRef = jobsRef.startAfter(startAfter);
    }

    if (filters) {
        if (filters.role) {
            jobsRef = jobsRef.where('role', '==', filters.role);
        }
        if (filters.employmentType) {
            jobsRef = jobsRef.where('employmentType', '==', filters.employmentType);
        }
        if (filters.country && !filters.keyword) { // @todo: cant do 2 array-contains-any in 1 query.
            jobsRef = jobsRef.where('countryList', 'array-contains-any', [filters.country, REMOTE]);
        }
        if (filters.country && filters.keyword) { // @todo: cant do 2 array-contains-any in 1 query.
            jobsRef = jobsRef.where('country', '==', filters.country);
        }
        if (filters.keyword) {
            jobsRef = jobsRef.where('keywordsLowercase', 'array-contains-any', [filters.keyword.toLowerCase()]);
        }
    }

    const snapshot = await jobsRef.get();
    const jobs = snapshot && snapshot.docs.map(d => {
        const data = Object.assign({id: d.id}, d.data());
        return getSerializableJob(data);
    });

    res.status(200).json(jobs);
    res.end();
}
