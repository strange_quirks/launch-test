import { db } from "lib/firebase";
import { getSerializableJob} from "src/helpers/serializable";
import { STATUS_PENDING } from "src/helpers/constants";

export default async function pendingJob(req, res) {
    let jobsRef = db.collection("jobs").where('status', '==', STATUS_PENDING).orderBy('startTimestamp', 'asc').limit(1);

    const snapshot = await jobsRef.get();
    const jobs = snapshot && snapshot.docs.map(d => {
        const data = Object.assign({id: d.id}, d.data());
        return getSerializableJob(data);
    });

    res.status(200).json(jobs.length > 0 ? jobs[0] : {});
    res.end();
}
