import { db } from "lib/firebase";
import { getSerializableCompany } from "src/helpers/serializable";

export default async function findCompanies(req, res) {
    const { limit } = req.body;

    let ref = db.collection("companies").orderBy('createdTimestamp', 'desc').limit(limit);

    const snapshot = await ref.get();
    const companies = snapshot && snapshot.docs.map(d => {
        const data = Object.assign({id: d.id}, d.data());
        return getSerializableCompany(data);
    });

    res.status(200).json(companies);
    res.end();
}
