import { db } from "lib/firebase";
import { getSerializableCompany } from "src/helpers/serializable";

export default async function allCompanies(req, res) {
    let ref = db.collection("companies");

    const snapshot = await ref.get();
    const companies = snapshot && snapshot.docs.map(d => {
        const data = Object.assign({ id: d.id }, d.data());
        return getSerializableCompany(data);
    });

    res.status(200).json(companies);
    res.end();
}
