import React, { Component } from 'react';
import Tag from 'antd/lib/tag';
import Typography from 'antd/lib/typography';
import Statistic from 'antd/lib/statistic';
import Button from 'antd/lib/button';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import EditorRenderer from "src/components/common/EditorRenderer";
import ApplyNow from "src/components/jobs/ApplyNow";
import { EnvironmentOutlined, FlagOutlined, ClockCircleOutlined, UserOutlined, LaptopOutlined } from '@ant-design/icons';
import IssueModal from "src/components/issues/IssueModal";
import JobDetailsHead from "src/components/heads/JobDetailsHead";
import SocialShare from "src/components/common/SocialShare";
import { STATUS_DELETED, STATUS_WAITING, STATUS_PENDING } from 'src/helpers/constants';
import { getKeywordUrl, getCompanyUrlByKey, getRootUrl, getCategoryUrl, getCountryUrl } from 'src/helpers/helpers';
import { format, differenceInCalendarDays } from 'date-fns'
import SimilarJobs from "src/components/jobs/SimilarJobs";
import Link from "next/link";
import JobStructuredData from "src/components/jobs/JobStructuredData";
import { REMOTE } from "src/helpers/africanCountries";
import styles from 'src/components/styles/JobDetails.module.css';

class JobDetails extends Component {
    state = {
        issueModalOpen: false,
    }

    getTags() {
        const tags = [];
        if (this.props.job.country) {
            tags.push(
                <Link href={getCountryUrl(this.props.job.country)} key="location">
                    <a href={getCountryUrl(this.props.job.country)}>
                        <Tag className={styles.jobDetails__tagsTag}><EnvironmentOutlined/> {this.props.job.city ? `${this.props.job.city}, ` : ''}{this.props.job.country.toUpperCase()}</Tag>
                    </a>
                </Link>
            );
        }
        if (this.props.job.countryList.includes(REMOTE)) {
            tags.push(<Tag key="remote" className={styles.jobDetails__tagsTag}><LaptopOutlined/> Remote</Tag>);
        }
        tags.push(
            <Link href={getCategoryUrl(this.props.job.role)} key="role">
                <a href={getCategoryUrl(this.props.job.role)}>
                    <Tag className={styles.jobDetails__tagsTag}><UserOutlined/> {this.props.job.role}</Tag>
                </a>
            </Link>
        );
        if (this.props.job.employmentType) {
            tags.push(<Tag key="employmentType" className={styles.jobDetails__tagsTag}><ClockCircleOutlined/> {this.props.job.employmentType}</Tag>);
        }

        let keywords = [];
        if (this.props.job.keywords && this.props.job.keywords.length >= 0) {
            keywords = this.props.job.keywords.map((keyword) =>
                <Link href={getKeywordUrl(keyword)} key={keyword}>
                    <a href={getKeywordUrl(keyword)}>
                        <Tag key={keyword} className={styles.jobDetails__tagsTag}>
                            {keyword}
                        </Tag>
                    </a>
                </Link>);
            keywords = <div className={styles.jobDetails__keywords}>{keywords}</div>;
        }

        return (
            <div className={styles.jobDetails__tags}>
                {tags}
                {keywords}
            </div>
        );
    }

    getSalary() {
        if (!this.props.job.baseSalary) {
            return null;
        }

        return (
            <div className={styles.jobDetails__salary}>
                <Statistic
                    title="Annual base salary (This could vary from candidate to candidate)"
                    value={this.props.job.baseSalary}
                    precision={0}
                    prefix={this.props.job.currency}
                    valueStyle={{ fontSize: '14px', fontWeight: 'bold' }}
                />
            </div>
        );
    }

    getDaysLeft() {
        const daysRemaining = differenceInCalendarDays(new Date(this.props.job.endDate), new Date());
        if (daysRemaining <= 1) {
            return <strong style={{ color: '#ff4d4f' }}>Last day left to apply</strong>;
        }

        return <strong>{daysRemaining} days left to apply</strong>;
    }

    render() {
        const { id, title, description, applicationMethod, applicationDetails, companyLogo, companyName, companyDescription, endDate, createdAt } = this.props.job;
        const stats = this.props.stats;
        let endDateDate = new Date(endDate);
        endDateDate.setMonth(endDateDate.getMonth() + 2); // @todo temp show lots more jobs.
        const isInactive = new Date() > new Date(endDateDate);

        const companyUrl = getCompanyUrlByKey(companyName.trim().replace(/\s/g, "-").toLowerCase());

        return (
            <AppContainer>
                <JobDetailsHead job={this.props.job} />
                <JobStructuredData job={this.props.job} />
                <AppHeader />
                <div className={styles.jobDetails}>
                    <div className={styles.jobDetails__job}>
                        <Typography.Title>{title}</Typography.Title>
                        {!isInactive && <p className={styles.jobDetails__jobDate}>
                            Posted on {format(new Date(createdAt), 'MMMM do, YYY')} | {this.getDaysLeft()}{stats && stats.views > 1 && ` | ${stats.views} views`}
                        </p>}
                        {this.getTags()}
                        {this.getSalary()}

                        <EditorRenderer description={JSON.parse(description)} />
                        <div className={styles.jobDetails__jobApply}>
                            {applicationDetails && !isInactive && <div className={styles.jobDetails__jobApplicationDetails}>
                                {applicationDetails}
                            </div>}
                            <ApplyNow jobId={id} applicationMethod={applicationMethod} isInactive={isInactive}/>
                        </div>
                    </div>
                    <div className={styles.jobDetails__right}>
                        <div className={styles.jobDetails__company}>
                            <div className={styles.jobDetails__companyHeader}>
                                <Link href={companyUrl}>
                                    <a href={companyUrl}>
                                        <div className={styles.jobDetails__companyHeaderImage}>
                                            <img
                                                loading="lazy"
                                                width="80"
                                                src={companyLogo.imageUrl}
                                                alt={`${companyName} jobs logo`}
                                            />
                                        </div>
                                    </a>
                                </Link>
                                <div>
                                    <Link href={companyUrl}><a href={companyUrl}><Typography.Title level={3}>{companyName}</Typography.Title></a></Link>
                                </div>
                            </div>
                            {companyDescription && <div className={styles.jobDetails__companyDescription}>
                                {companyDescription}
                            </div>}
                        </div>
                        {!isInactive && <SocialShare />}
                        <div>
                            <Button title="Report this job post" size="small" danger={true} onClick={() => this.setState({ issueModalOpen: true })}>
                                <FlagOutlined /> Report this job
                            </Button>
                            <IssueModal jobId={id} isOpen={this.state.issueModalOpen} onClose={() => this.setState({ issueModalOpen: false })}/>
                        </div>
                        <SimilarJobs role={this.props.job.role} currentJobId={this.props.job.id}/>
                    </div>
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    const jobId = query.job.split("_").pop();
    if (!jobId) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    const response = await fetch(`${getRootUrl()}/api/job/${jobId}`);
    const jobData = await response.json();
    if (!jobData) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    } else {
        if ([STATUS_DELETED, STATUS_WAITING, STATUS_PENDING].includes(jobData.status)) {
            res.writeHead(302, {
                Location: '/'
            });
            res.end();
        }

        const responseStats = await fetch(`${getRootUrl()}/api/job/${jobId}/stats`);
        const statsData = await responseStats.json();

        if (process.env.NODE_ENV !== 'development') {
            await fetch(`${getRootUrl()}/api/job/${jobId}/incrementViews`);
        }

        return {
            props: {
                job: jobData,
                stats: statsData,
            }
        };
    }
};

export default JobDetails;
