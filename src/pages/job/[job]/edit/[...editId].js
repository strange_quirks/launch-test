import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import PostJobForm from "src/components/post/PostJobForm";
import PostHeaderEdit from "src/components/post/PostHeaderEdit";
import { decrypt } from 'src/helpers/helpers';
import message from 'antd/lib/message';
import {STATUS_ACTIVE, STATUS_DELETED, STATUS_EXPIRED, STATUS_PENDING, STATUS_WAITING} from "src/helpers/constants";
import styles from 'src/components/styles/PostJob.module.css';
import jobEditStyles from 'src/components/styles/JobEdit.module.css';
import Statistic from "antd/lib/statistic";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Typography from "antd/lib/typography";
import Button from "antd/lib/button";
import { getJobUrl, getRootUrl } from 'src/helpers/helpers';

class JobEdit extends Component {
    handleSave = async(formData) => {
        const url = `/api/job/${this.props.job.id}/edit`
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify(Object.assign({ ejid: this.props.job.encryptedJobId }, formData)),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => message.success('Job post successfully updated!'));
    }

    getAdditionalActions() {
        const additionalActions = [];
        if (this.props.stats) {
            additionalActions.push(
                <div className={jobEditStyles.statistics}>
                    <Typography.Title level={3} style={{ paddingBottom: '20px' }}>Statistics</Typography.Title>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Statistic title="Views" value={this.props.stats.views} />
                        </Col>
                        <Col span={12}>
                            <Statistic title="Application clicks" value={this.props.stats.applies} />
                        </Col>
                    </Row>
                </div>
            );
        }

        if ([STATUS_ACTIVE, STATUS_EXPIRED].includes(this.props.job.status)) {
            additionalActions.push(
                <Button key="view" size="large" block={true} target="_blank" rel="noopener" href={`${getRootUrl()}${getJobUrl(this.props.job.title, this.props.job.id)}`}>
                    View job
                </Button>
            );
        }

        if (additionalActions.length === 0) {
            return null;
        }

        return (
            <div className={styles.postJobForm__card}>
                {additionalActions}
            </div>
        );
    }

    getStatus() {
        switch (this.props.job.status) {
            case STATUS_ACTIVE:
                return 'Online';
            case STATUS_EXPIRED:
                return 'Expired';
            case STATUS_PENDING:
                return 'Pending';
            case STATUS_DELETED:
            case STATUS_WAITING:
            default:
                return 'Contact support';
        }
    }

    render() {
        return (
            <AppContainer>
                <AppHeader />
                <div className={styles.postJob}>
                    <PostHeaderEdit status={this.getStatus()}/>
                    <PostJobForm
                        job={this.props.job}
                        onSave={this.handleSave}
                        additionalActions={this.getAdditionalActions()}
                    />
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    const jobId = query.job.split("_").pop();
    if (!jobId) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    const response = await fetch(`${getRootUrl()}/api/job/${jobId}`);
    const jobData = await response.json();
    if (!jobData) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    } else {
        let editId = query.editId;
        if (Array.isArray(editId)) { // For some reason on production its an array. Maybe because of the "/".
            editId = editId.join('/');
        }
        if (jobData.id !== decrypt(editId)) {
            res.writeHead(302, {
                Location: '/'
            });
            res.end();
        }

        if ([STATUS_DELETED].includes(jobData.status)) {
            res.writeHead(302, {
                Location: '/'
            });
            res.end();
        }


        const responseStats = await fetch(`${getRootUrl()}/api/job/${jobId}/stats`);
        const statsData = await responseStats.json();

        return {
            props: {
                job: jobData,
                stats: statsData,
            }
        };
    }
}

export default JobEdit;
