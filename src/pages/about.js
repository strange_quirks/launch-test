import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import styles from 'src/components/styles/About.module.css';
import Typography from "antd/lib/typography";
import Link from "next/link";
import Button from "antd/lib/button";
import { getPostJobUrl } from 'src/helpers/helpers';
import AboutHead from "src/components/heads/AboutHead";

class About extends Component {
    render() {
        return (
            <AppContainer>
                <AboutHead />
                <AppHeader />
                <div className={styles.aboutContainer}>
                    <Typography.Title level={1}>👋 About Us</Typography.Title>
                    <Typography className={styles.about__contentText}>LaunchAfrica is the leading platform to discover new career opportunities in startups and tech companies in Africa. We also cater to companies looking to hire talent from abroad to work remotely.</Typography>

                    <Typography.Title level={3}>🔍 Looking For A Job?</Typography.Title>
                    <Typography className={styles.about__contentText}>
                        LaunchAfrica provides an overview of career opportunities in the startup and tech space in Africa.<br/>
                        Job searches can be filtered by country, employment type (e.g. full-time, internship, etc), remote, as well as categories (e.g. engineering, sales, HR, etc).<br/>
                        <img src="/images/about/categories.png" alt="Job categories" className={styles.about__imageCategories}/>
                        <img src="/images/about/filters.png" alt="Job filters" className={styles.about__imageFilters} />
                        Or, find your next dream job by simply using the search bar on the <Link href="/"><a href="/">Home Page</a></Link>.
                        <img src="/images/about/search.png" alt="Job search" className={styles.about__imageSearch} />
                        <div className={styles.about__action}>
                            <Link href="/" >
                                <Button href="/" aria-label="Find your dream job" size="large">
                                    <strong>Find your dream job</strong>
                                </Button>
                            </Link>
                        </div>
                    </Typography>

                    <Typography.Title level={3}>📝 Looking To Hire Top Talent?</Typography.Title>
                    <Typography className={styles.about__contentText}>
                        LaunchAfrica welcomes new or established startups and tech companies in Africa to reach a global audience of skilled professionals. Each job is reviewed by our team to ensure quality and absolutely FREE (limited time).
                        Simply click the button below to get started and post your job today.
                        <div className={styles.about__action}>
                            <Link href={getPostJobUrl()} >
                                <Button href={getPostJobUrl()} aria-label="Post a job for free" size="large">
                                    <strong>Post a job for free</strong>
                                </Button>
                            </Link>
                        </div>
                    </Typography>

                    <Typography.Title level={3}>📧 Contact Us</Typography.Title>
                    <Typography className={styles.about__contentText}>
                        You can reach us at support@launchafrica.io or give us a shout on <a href="https://twitter.com/africa_launch" target="_blank" rel="noopener">Twitter</a> 😊.
                    </Typography>
                </div>
            </AppContainer>
        );
    }
}

export default About;
