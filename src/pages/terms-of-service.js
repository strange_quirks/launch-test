import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";

class TermsOfService extends Component {
    render() {
        return (
            <AppContainer>
                <AppHeader />
                <div className="terms-container">
                    Terms of Service
                </div>
            </AppContainer>
        );
    }
}

export default TermsOfService;
