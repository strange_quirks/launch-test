import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import Button from 'antd/lib/button';
import Result from 'antd/lib/result';
import { MinusCircleOutlined } from "@ant-design/icons";
import JobCardList from "src/components/jobs/JobCardList";
import CategoryHeader from "src/components/common/CategoryHeader";
import { QUERY_LIMIT } from "src/helpers/constants";
import Link from "next/link";
import CategoryHead from "src/components/heads/CategoryHead";
import styles from 'src/components/styles/Category.module.css';
import FiltersContainer from "src/components/jobs/FiltersContainer";
import { getRootUrl } from "src/helpers/helpers";

class Category extends Component {
    state = {
        jobs: this.props.jobs,
        hasMore: true,
        filters: {},
    };

    componentDidUpdate(prevProps) {
        if (prevProps.category !== this.props.category) {
            this.setState({
                hasMore: true,
                jobs: [],
                onlyRemoteJobs: false,
            }, () => this.getJobs())
        }
    }

    getJobs = () => {
        if (!this.state.hasMore) {
            return;
        }

        this.firestoreGetJobs().then((data) => {
            this.setState(data)
        })
    }

    async firestoreGetJobs() {
        const filters = {
            role: this.props.category
        };
        if (this.state.filters.employmentType) {
            filters.employmentType = this.state.filters.employmentType;
        }
        if (this.state.filters.country) {
            filters.country = this.state.filters.country;
        }

        const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
            method: 'POST',
            body: JSON.stringify({
                limit: QUERY_LIMIT,
                filters,
                startAfter: this.state.jobs && this.state.jobs.length > 0 ? this.state.jobs[this.state.jobs.length - 1].startTimestamp : null
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const jobsData = await responseJobs.json();

        const jobs = this.state.jobs;
        jobsData.forEach(job => {
            jobs.push(job);
        });

        return {
            jobs,
            hasMore: jobsData.length > 0,
        };
    }

    handleUpdate = (filters) => {
        this.setState({ filters, jobs: [], hasMore: true }, () => {
            this.getJobs();
        });
    }

    getEmptyState() {
        return (
            <Result
                icon={<MinusCircleOutlined />}
                title="No jobs found"
                extra={
                    <Link href="/post/job">
                        <Button type="primary" href="/post/job">
                            Post a job for free
                        </Button>
                    </Link>
                }
            />
        );
    }

    render() {
        return (
            <AppContainer>
                <CategoryHead category={this.props.category} />
                <AppHeader />
                <div className={styles.categoryContainer}>
                    <CategoryHeader category={this.props.category} />
                    <FiltersContainer onUpdate={this.handleUpdate} showRoleFilter={false} />
                    {this.state.jobs && this.state.jobs.length > 0 && <JobCardList jobs={this.state.jobs} onLoadMore={this.getJobs}/>}
                    {!this.state.jobs || this.state.jobs.length === 0 && this.getEmptyState()}
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    let category = query.category;
    if (!category) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    category = category.replace('_', ' ');
    const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
        method: 'POST',
        body: JSON.stringify({
            limit: QUERY_LIMIT,
            filters: {
                role: category,
            }
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const jobsData = await responseJobs.json();

    return {
        props: {
            jobs: jobsData,
            category,
        }
    }
};

export default Category;
