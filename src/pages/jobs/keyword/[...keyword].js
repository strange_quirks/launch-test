import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import Button from 'antd/lib/button';
import Result from 'antd/lib/result';
import { MinusCircleOutlined } from "@ant-design/icons";
import JobCardList from "src/components/jobs/JobCardList";
import KeywordHeader from "src/components/common/KeywordHeader";
import { QUERY_LIMIT } from "src/helpers/constants";
import Link from "next/link";
import { getPostJobUrl, getRootUrl } from 'src/helpers/helpers';
import KeywordHead from "src/components/heads/KeywordHead";
import styles from 'src/components/styles/Keyword.module.css';
import FiltersContainer from "src/components/jobs/FiltersContainer";

class Keyword extends Component {
    state = {
        jobs: this.props.jobs,
        limit: QUERY_LIMIT,
        hasMore: true,
        filters: {},
    };

    componentDidUpdate(prevProps) {
        if (prevProps.keyword !== this.props.keyword) {
            this.setState({
                hasMore: true,
                jobs: [],
            }, () => this.getJobs())
        }
    }

    getJobs = () => {
        if (!this.state.hasMore) {
            return;
        }

        this.firestoreGetJobs().then((data) => {
            this.setState(data)
        })
    }

    async firestoreGetJobs() {
        const filters = {
            keyword: this.props.keyword,
            role: this.props.category
        };
        if (this.state.filters.role) {
            filters.role = this.state.filters.role;
        }
        if (this.state.filters.employmentType) {
            filters.employmentType = this.state.filters.employmentType;
        }
        if (this.state.filters.country) {
            filters.country = this.state.filters.country;
        }

        const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
            method: 'POST',
            body: JSON.stringify({
                limit: QUERY_LIMIT,
                filters,
                startAfter: this.state.jobs && this.state.jobs.length > 0 ? this.state.jobs[this.state.jobs.length - 1].startTimestamp : null
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const jobsData = await responseJobs.json();

        const jobs = this.state.jobs;
        jobsData.forEach(job => {
            jobs.push(job);
        });

        return {
            jobs,
            hasMore: jobsData.length > 0,
        };
    }

    handleUpdate = (filters) => {
        this.setState({ filters, jobs: [], hasMore: true }, () => {
            this.getJobs();
        });
    }

    getEmptyState() {
        return (
            <Result
                icon={<MinusCircleOutlined />}
                title={`There are no available jobs for "${this.props.keyword}"`}
                extra={
                    <Link href={getPostJobUrl()}>
                        <Button type="primary" href={getPostJobUrl()}>
                            Post a job for free
                        </Button>
                    </Link>
                }
            />
        );
    }

    render() {
        return (
            <AppContainer>
                <KeywordHead keyword={this.props.keywordDisplayed} />
                <AppHeader />
                <div className={styles.keywordContainer}>
                    <KeywordHeader keyword={this.props.keywordDisplayed} />
                    <FiltersContainer onUpdate={this.handleUpdate}/>
                    {this.state.jobs && this.state.jobs.length > 0 && <JobCardList jobs={this.state.jobs} onLoadMore={this.getJobs}/>}
                    {!this.state.jobs || this.state.jobs.length === 0 && this.getEmptyState()}
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    let keyword = query.keyword[0];
    if (query?.keyword.length > 1) {
        keyword = query.keyword.join('/');
    }

    if (!keyword) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
        method: 'POST',
        body: JSON.stringify({
            limit: QUERY_LIMIT,
            filters: {
                keyword: decodeURIComponent(keyword.replace(/-/g, ' '))
            }
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const jobsData = await responseJobs.json();

    return {
        props: {
            jobs: jobsData,
            keyword,
            keywordDisplayed: decodeURIComponent(keyword.replace(/-/g, ' ')),
        }
    }
};

export default Keyword;
