import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import Button from 'antd/lib/button';
import Result from 'antd/lib/result';
import { MinusCircleOutlined } from "@ant-design/icons";
import JobCardList from "src/components/jobs/JobCardList";
import CountryKeywordHeader from "src/components/common/CountryKeywordHeader";
import { QUERY_LIMIT } from "src/helpers/constants";
import Link from "next/link";
import CountryKeywordHead from "src/components/heads/CountryKeywordHead";
import styles from 'src/components/styles/Category.module.css';
import { getRootUrl } from "src/helpers/helpers";

class CountryKeywordDetails extends Component {
    state = {
        jobs: this.props.jobs,
        limit: QUERY_LIMIT,
        hasMore: true,
    };

    componentDidUpdate(prevProps) {
        if (prevProps.keyword !== this.props.keyword) {
            this.setState({
                hasMore: true,
                jobs: [],
            }, () => this.getJobs())
        }
    }

    getJobs = () => {
        if (!this.state.hasMore) {
            return;
        }

        this.firestoreGetJobs().then((data) => {
            this.setState(data)
        })
    }

    async firestoreGetJobs() {
        const filters = {
            country: this.props.country,
            keyword: decodeURIComponent(this.props.keyword.replace(/-/g, ' '))
        };

        const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
            method: 'POST',
            body: JSON.stringify({
                limit: QUERY_LIMIT,
                filters,
                startAfter: this.state.jobs && this.state.jobs.length > 0 ? this.state.jobs[this.state.jobs.length - 1].startTimestamp : null
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const jobsData = await responseJobs.json();

        const jobs = this.state.jobs;
        jobsData.forEach(job => {
            jobs.push(job);
        });

        return {
            jobs,
            hasMore: jobsData.length > 0,
        };
    }

    getEmptyState() {
        return (
            <Result
                icon={<MinusCircleOutlined />}
                title="There are no active jobs in this country for this keyword"
                extra={
                    <Link href="/post/job">
                        <Button type="primary" href="/post/job">
                            Post a job for free
                        </Button>
                    </Link>
                }
            />
        );
    }

    render() {
        return (
            <AppContainer>
                <CountryKeywordHead keyword={this.props.keyword} country={this.props.country} />
                <AppHeader />
                <div className={styles.categoryContainer}>
                    <CountryKeywordHeader keyword={this.props.keyword} country={this.props.country} />
                    {this.state.jobs && this.state.jobs.length > 0 && <JobCardList jobs={this.state.jobs} onLoadMore={this.getJobs}/>}
                    {!this.state.jobs || this.state.jobs.length === 0 && this.getEmptyState()}
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    let keyword = query.keyword;
    let country = query.country;
    if (!keyword || !keyword[0] || !country) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    country = country.replace('_', ' ');

    keyword = keyword[0];
    if (keyword.length > 1) {
        keyword = query.keyword.join('/');
    }

    const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
        method: 'POST',
        body: JSON.stringify({
            limit: 2,
            filters: {
                keyword: decodeURIComponent(keyword.replace(/-/g, ' ')),
                country: country,
            }
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const jobsData = await responseJobs.json();

    return {
        props: {
            jobs: jobsData,
            keyword,
            country,
        }
    }
};

export default CountryKeywordDetails;
