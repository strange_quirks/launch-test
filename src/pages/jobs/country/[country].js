import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import Button from 'antd/lib/button';
import Result from 'antd/lib/result';
import { MinusCircleOutlined } from "@ant-design/icons";
import JobCardList from "src/components/jobs/JobCardList";
import CountryHeader from "src/components/common/CountryHeader";
import { QUERY_LIMIT } from "src/helpers/constants";
import Link from "next/link";
import {getPostJobUrl, getRootUrl} from 'src/helpers/helpers';
import CountryHead from "src/components/heads/CountryHead";
import styles from 'src/components/styles/Country.module.css';
import FiltersContainer from "src/components/jobs/FiltersContainer";

class Country extends Component {
    state = {
        jobs: this.props.jobs,
        limit: QUERY_LIMIT,
        hasMore: true,
        filters: {},
    };

    componentDidUpdate(prevProps) {
        if (prevProps.country !== this.props.country) {
            this.setState({
                hasMore: true,
                jobs: [],
            }, () => this.getJobs())
        }
    }

    getJobs = () => {
        if (!this.state.hasMore) {
            return;
        }

        this.firestoreGetJobs().then((data) => {
            this.setState(data)
        })
    }

    async firestoreGetJobs() {
        const filters = {
            country: this.props.country
        };
        if (this.state.filters.employmentType) {
            filters.employmentType = this.state.filters.employmentType;
        }
        if (this.state.filters.role) {
            filters.role = this.state.filters.role;
        }

        const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
            method: 'POST',
            body: JSON.stringify({
                limit: QUERY_LIMIT,
                filters,
                startAfter: this.state.jobs && this.state.jobs.length > 0 ? this.state.jobs[this.state.jobs.length - 1].startTimestamp : null
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const jobsData = await responseJobs.json();

        const jobs = this.state.jobs;
        jobsData.forEach(job => {
            jobs.push(job);
        });

        return {
            jobs,
            hasMore: jobsData.length > 0,
        };
    }

    handleUpdate = (filters) => {
        this.setState({ filters, jobs: [], hasMore: true }, () => {
            this.getJobs();
        });
    }

    getEmptyState() {
        return (
            <Result
                icon={<MinusCircleOutlined />}
                title="There are no available jobs in this country"
                extra={
                    <Link href={getPostJobUrl()}>
                        <Button type="primary" href={getPostJobUrl()}>
                            Post a job for free
                        </Button>
                    </Link>
                }
            />
        );
    }

    render() {
        return (
            <AppContainer>
                <CountryHead country={this.props.country} />
                <AppHeader />
                <div className={styles.countryContainer}>
                    <CountryHeader country={this.props.country} />
                    <FiltersContainer onUpdate={this.handleUpdate} showCountryFilter={false} />
                    {this.state.jobs && this.state.jobs.length > 0 && <JobCardList jobs={this.state.jobs} onLoadMore={this.getJobs}/>}
                    {!this.state.jobs || this.state.jobs.length === 0 && this.getEmptyState()}
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    let country = query.country;
    if (!country) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    country = country.replace('_', ' ');

    const responseJobs = await fetch(`${getRootUrl()}/api/jobs/find`, {
        method: 'POST',
        body: JSON.stringify({
            limit: QUERY_LIMIT,
            filters: {
                country: country,
            }
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const jobsData = await responseJobs.json();

    return {
        props: {
            jobs: jobsData,
            country,
        }
    }
};

export default Country;
