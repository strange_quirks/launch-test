import React, { Component } from 'react';
import Button from 'antd/lib/button';
import Result from 'antd/lib/result';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import CompanyDetailsHead from "src/components/heads/CompanyDetailsHead";
import { QUERY_LIMIT } from "src/helpers/constants";
import { MinusCircleOutlined } from "@ant-design/icons";
import Link from "next/link";
import CompanyHeader from "src/components/common/CompanyHeader";
import JobCardList from "src/components/jobs/JobCardList";
import styles from 'src/components/styles/Company.module.css';
import { getRootUrl } from "src/helpers/helpers";

class CompanyDetails extends Component {
    state = {
        jobs: this.props.jobs,
        hasMore: true,
    };

    getJobs = () => {
        if (!this.state.hasMore) {
            return;
        }

        this.firestoreGetJobs().then((data) => {
            this.setState(data)
        })
    }

    async firestoreGetJobs() {
        const responseJobs = await fetch(`${getRootUrl()}/api/company/${this.props.company.key}/jobs`, {
            method: 'POST',
            body: JSON.stringify({
                name: this.props.company.name.trim(), // @todo Use the companyKey in the api somehow and remove this.
                limit: QUERY_LIMIT,
                startAfter: this.state.jobs && this.state.jobs.length > 0 ? this.state.jobs[this.state.jobs.length - 1].startTimestamp : null
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const jobsData = await responseJobs.json();

        const jobs = this.state.jobs;
        jobsData.forEach(job => {
            jobs.push(job);
        });

        return {
            jobs,
            hasMore: jobsData.length > 0,
        };
    }

    getEmptyState() {
        return (
            <Result
                icon={<MinusCircleOutlined />}
                title="There are no available jobs in this company"
                extra={
                    <Link href="/post/job">
                        <Button type="primary" href="/post/job">
                            Post a job for free
                        </Button>
                    </Link>
                }
            />
        );
    }

    render() {
        return (
            <AppContainer>
                <CompanyDetailsHead company={this.props.company} />
                <AppHeader />
                <div className={styles.companyContainer}>
                    <CompanyHeader company={this.props.company} />
                    {this.state.jobs && this.state.jobs.length > 0 && <JobCardList jobs={this.state.jobs} onLoadMore={this.getJobs}/>}
                    {!this.state.jobs || this.state.jobs.length === 0 && this.getEmptyState()}
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    const company = query.company;
    if (!company) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    const response = await fetch(`${getRootUrl()}/api/company/${company.trim()}`);
    const companyData = await response.json();
    if (!companyData) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    } else {
        const responseJobs = await fetch(`${getRootUrl()}/api/company/${company.trim()}/jobs`, {
            method: 'POST',
            body: JSON.stringify({
                name: companyData.name, // @todo Use the companyKey in the api somehow and remove this.
                limit: QUERY_LIMIT,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const jobsData = await responseJobs.json();

        return {
            props: {
                company: companyData,
                jobs: jobsData,
            }
        };
    }
}

export default CompanyDetails;
