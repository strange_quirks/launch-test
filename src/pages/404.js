import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import styles from 'src/components/styles/Index.module.css';
import AppHeader from "src/components/common/AppHeader";
import Button from 'antd/lib/button';
import Result from 'antd/lib/result';

class Custom404 extends Component {
    render() {
        return (
            <AppContainer>
                <AppHeader />
                <div className={styles.indexContainer}>
                    <Result
                        status="404"
                        title={<strong>404</strong>}
                        subTitle="Sorry, the page you visited does not exist."
                        extra={<Button type="primary">Back Home</Button>}
                    />,
                </div>
            </AppContainer>
        );
    }
}

export default Custom404;
