import { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import AppHeader from "src/components/common/AppHeader";
import PostHeader from "src/components/post/PostHeader";
import PostJobForm from "src/components/post/PostJobForm";
import { getJobPostCompletedUrl, encrypt } from 'src/helpers/helpers';
import PostJobHead from "src/components/heads/PostJobHead";
import List from 'antd/lib/list';
import { FileTwoTone, CheckCircleTwoTone, CalendarTwoTone, EditTwoTone, QuestionCircleTwoTone, InfoCircleTwoTone } from "@ant-design/icons";
import styles from 'src/components/styles/PostJob.module.css';

class Job extends Component {
    handleSave = async(formData) => {
        const url = `/api/jobs/post`
        const result = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return await result.json();
    }

    handleAfterSave = (jobId) => {
        window.location.href = getJobPostCompletedUrl(encrypt(jobId));
    }

    getAdditionalInfo() {
        const data = [
            <span><InfoCircleTwoTone /> Posting job posts is completely FREE! - Limited time offer.</span>,
            <span><FileTwoTone /> Fill out all required fields</span>,
            <span><CheckCircleTwoTone /> Jobs will be reviewed for quality control before they are online</span>,
            <span><CalendarTwoTone /> Jobs will stay online for 60 days</span>,
            <span><EditTwoTone /> After you submit your job post, you will get a URL you can use for future edits</span>,
            <span><QuestionCircleTwoTone /> For any questions or issues, simply fill out <a target="_blank" rel="noopener" href="https://qrfeedbackcreator.com/form/LguXLR6ko">this form</a> or send us an email here: support@launchafrica.io</span>,
        ];

        return (
            <div>
                <List
                    size="small"
                    bordered={false}
                    dataSource={data}
                    renderItem={item => <List.Item>{item}</List.Item>}
                />
            </div>
        );
    }

    render() {
        return (
            <AppContainer>
                <PostJobHead />
                <AppHeader/>
                <div className={styles.postJob}>
                    <PostHeader/>
                    <PostJobForm
                        onSave={this.handleSave}
                        onAfterSave={this.handleAfterSave}
                        additionalActions={this.getAdditionalInfo()}
                    />
                </div>
            </AppContainer>
        );
    }
}

export default Job;
