import React, { Component } from 'react';
import AppContainer from "src/components/AppContainer";
import Button from 'antd/lib/button';
import Alert from 'antd/lib/alert';
import Result from 'antd/lib/result';
import Link from "next/link";
import AppHeader from "src/components/common/AppHeader";
import { JOB_COMPLETION_ENCRYPTED_PARAM } from 'src/helpers/constants';
import { decrypt, getJobPostEditUrl, getPostJobUrl, getRootUrl } from 'src/helpers/helpers';
import styles from 'src/components/styles/Completed.module.css';

class Completed extends Component {
    render() {
        return (
            <AppContainer>
                <AppHeader/>
                <div>
                    <Result
                        className={styles.postJobCompletedResultsContainer}
                        status="success"
                        title="Job Post Successfully Submitted"
                        subTitle="In order to ensure a high level of quality, we will review your job post before it is taken online."
                        extra={[
                            <Link href={getPostJobUrl()}>
                                <Button type="primary" href={getPostJobUrl()} rel="noopener" target="_blank">
                                    Post another job
                                </Button>
                            </Link>
                        ]}
                    >
                        <Alert
                            message={<strong>Important!</strong>}
                            description={
                                <div>
                                    <p>Save this URL to edit your job later.</p>
                                    <div><strong>{getRootUrl()}{this.props.editUrl}</strong></div>
                                </div>
                            }
                            type="info"
                            showIcon={true}
                        />
                    </Result>
                </div>
            </AppContainer>
        );
    }
}

export async function getServerSideProps({ query, res }) {
    if (!query[JOB_COMPLETION_ENCRYPTED_PARAM]) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    const jobId = decrypt(query[JOB_COMPLETION_ENCRYPTED_PARAM]);
    if (!jobId) {
        res.writeHead(302, {
            Location: '/'
        });
        res.end();
    }

    const response = await fetch(`${getRootUrl()}/api/job/${jobId}`);
    const jobData = await response.json();
    return {
        props: {
            job: jobData,
            editUrl: getJobPostEditUrl(jobData.id, jobData.title)
        }
    };
};

export default Completed;
