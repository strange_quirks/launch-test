export const STATUS_ACTIVE = 'active';
export const STATUS_PENDING = 'pending';
export const STATUS_WAITING = 'waiting'; // Waiting for a response from the client for example.
export const STATUS_EXPIRED = 'expired';
export const STATUS_DELETED = 'deleted';

export const JOB_COMPLETION_ENCRYPTED_PARAM = 'eId';

export const QUERY_LIMIT = 10;

export const PROVERBS = [
    'Teeth do not see poverty',
    'Only a fool tests the depth of a river with both feet',
    'Do not look where you fell, but where you slipped',
    'The best way to eat an elephant in your path is to cut him up into little pieces',
    'He who does not know one thing knows another',
    'Rain beats the leopards skin but it does not wash out the spots',
    'No matter how hot your anger is it cannot cook yams',
    'A roaring lion kills no game',
    'Do not call the forest that shelters you a jungle',
    'Rain does not fall on one roof alone',
    'Ears that do not listen to advice, accompany the head when it is chopped off',
    'Not everyone who chased the zebra caught it, but he who caught it, chased it',
    'The axe forgets but the tree remembers',
    'If you want to go fast, go alone. If you want to go far, go together',
    "When elephants fight, it's the grass that suffers",
];
