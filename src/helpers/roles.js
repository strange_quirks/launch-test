export const ROLE_ENGINEERING = 'Engineering';
export const ROLE_DESIGN = 'Design';
export const ROLE_PRODUCT = 'Product';
export const ROLE_ANALYTICS = 'Analytics';
export const ROLE_COPYWRITING = 'Copywriting';
export const ROLE_CUSTOMER_SUPPORT = 'Customer Support';
export const ROLE_FINANCE = 'Finance';
export const ROLE_BUSINESS = 'Business';
export const ROLE_MARKETING = 'Marketing';
export const ROLE_SALES = 'Sales';
export const ROLE_HR = 'HR';
export const ROLE_OPERATIONS = 'Operations';
export const ROLE_LEGAL = 'Legal';
export const ROLE_EXECUTIVE = 'Executive';

export const ROLES = [
    ROLE_ENGINEERING,
    ROLE_DESIGN,
    ROLE_PRODUCT,
    ROLE_ANALYTICS,
    ROLE_COPYWRITING,
    ROLE_CUSTOMER_SUPPORT,
    ROLE_FINANCE,
    ROLE_BUSINESS,
    ROLE_MARKETING,
    ROLE_SALES,
    ROLE_HR,
    ROLE_OPERATIONS,
    ROLE_LEGAL,
    ROLE_EXECUTIVE,
];

export const ROLE_ICON_NAME_MAPPINGS = {
    "Engineering": "engineering",
    "Design": "design",
    "Product": "product",
    "Analytics": "analytics",
    "Copywriting": "copywriting",
    "Customer Support": "customerSupport",
    "Finance": "finance",
    "Business": "business",
    "Marketing": "marketing",
    "Sales": "sales",
    "HR": "hr",
    "Operations": "operations",
    "Legal": "legal",
    "Executive": "executive",
};
