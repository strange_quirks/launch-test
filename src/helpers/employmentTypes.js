export const EMPLOYMENT_TYPES = [
    "Full-Time",
    "Part-Time",
    "Internship",
    "Contractor",
    "Temporary",
    "Freelance",
    "Volunteer"
];
