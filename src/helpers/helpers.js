import CryptoJS from 'crypto-js';
import { JOB_COMPLETION_ENCRYPTED_PARAM } from 'src/helpers/constants';

export function isEmail(email) {
    return /\S+@\S+\.\S+/.test(email)
}

export function getJobUrl(title, id) {
    return `/job/${encodeURIComponent(title.replace(/\s+/g, '-').toLowerCase())}_${id}`;
}

export function getCategoryUrl(category) {
    return `/jobs/category/${category.replace(/\s+/g, '_')}`;
}

export function getCountryCategoryUrl(country, category) {
    return `/jobs/country/${country.replace(/\s+/g, '_')}/category/${category.replace(/\s+/g, '_')}`;
}

export function getCountryKeywordUrl(country, keyword) {
    return `/jobs/country/${country.replace(/\s+/g, '_')}/keyword/${encodeURIComponent(keyword.replace(/\s+/g, '-'))}`;
}

export function getCompanyUrlByKey(key) {
    return `/company/${encodeURIComponent(key)}`;
}

export function getCountryUrl(country) {
    return `/jobs/country/${country.replace(/\s+/g, '_')}`;
}

export function getKeywordUrl(keyword) {
    return `/jobs/keyword/${encodeURIComponent(keyword.trim().replace(/\s/g, "-").toLowerCase())}`;
}

export function getRootUrl() {
    return process.env.NODE_ENV !== 'production' ? process.env.ROOT_URL : process.env.ROOT_URL_PRODUCTION;
}

export function getJobPostCompletedUrl(encryptedId) {
    return `/post/job/completed?${JOB_COMPLETION_ENCRYPTED_PARAM}=${encryptedId}`;
}

export function getJobPostEditUrl(jobId, title) {
    return `${getJobUrl(title, jobId)}/edit/${encrypt(jobId)}`
}

export function getPostJobUrl() {
    return `/post/job`;
}

export function getAboutUrl() {
    return `/about`;
}

export function encrypt(string, alsoEncodeURI = true) {
    let encrypted = CryptoJS.AES.encrypt(string, process.env.ENCRYPTION_KEY).toString();
    if (alsoEncodeURI) {
        encrypted = encodeURIComponent(encrypted);
    }

    return encrypted
}

export function decrypt(string, alsoDecodeURI = true) {
    let decrypted = string;
    if (alsoDecodeURI) {
        decrypted = decodeURIComponent(string);
    }

    return CryptoJS.AES.decrypt(decrypted, process.env.ENCRYPTION_KEY).toString(CryptoJS.enc.Utf8);
}

export function secondsToDateTime(seconds) {
    const date = new Date(1970, 0, 1); // Epoch
    date.setSeconds(seconds);
    return date;
}
