import { secondsToDateTime } from "src/helpers/helpers";

export function getSerializableJob(job) {
    if (job.updatedAt) {
        job.updatedAt = secondsToDateTime(job.updatedAt).toString();
    }

    if (job.createdAt.seconds) {
        job.createdAt = secondsToDateTime(job.createdAt.seconds).toString();
    } else {
        job.createdAt = new Date(job.createdAt).toString();
    }

    if (job.endDate.seconds) {
        job.endDate = secondsToDateTime(job.endDate.seconds).toString();
    } else {
        job.endDate = new Date(job.endDate).toString();
    }

    return job;
}

export function getSerializableCompany(company) {
    company.createdAt = secondsToDateTime(company.createdAt).toString();
    return company;
}
