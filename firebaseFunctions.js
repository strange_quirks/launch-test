const { join } = require('path');
const { https, firestore } = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const { default: next } = require('next');
const mailgun = require('mailgun-js');
const CryptoJS = require('crypto-js');
const algoliasearch = require('algoliasearch');
// @todo: Would be cool to be able to use import statements in here. Then we can use the helpers and not duplicate code.

const isDev = process.env.NODE_ENV !== 'production';
const nextjsDistDir = join('src', require('./src/next.config.js').distDir);

const nextjsServer = next({
    dev: isDev,
    conf: {
      distDir: nextjsDistDir,
    },
});
const nextjsHandle = nextjsServer.getRequestHandler();

exports.nextjsFunc = https.onRequest((req, res) => {
    return nextjsServer.prepare().then(() => nextjsHandle(req, res));
});

const algoliaClient = algoliasearch(process.env.ALGOLIA_APPLICATION_ID, process.env.ALGOLIA_ADMIN_KEY);

exports.onJobPostCreated = firestore.document('jobs/{id}').onCreate((snap) => {
    const job = snap.data();

    const email = CryptoJS.AES.decrypt(job.companyEmail, process.env.ENCRYPTION_KEY).toString(CryptoJS.enc.Utf8);
    if (!email.includes("@launchafrica.io")) {
        let jobEditUrl = process.env.ROOT_URL_PRODUCTION;
        jobEditUrl += `/job/${encodeURIComponent(job.title.replace(/\s+/g, '-').toLowerCase())}_${snap.id}`;
        jobEditUrl += `/edit/${encodeURIComponent(CryptoJS.AES.encrypt(snap.id, process.env.ENCRYPTION_KEY).toString())}`;

        const mg = mailgun({apiKey: process.env.MAILGUN_API_KEY, domain: "mail.launchafrica.io", host: "api.eu.mailgun.net"});
        const data = {
            from: 'Support <support@mail.launchafrica.io>',
            to: email,
            subject: `Job post created: ${job.title}`,
            text: 'Your job post has been created! We will quickly first verify it for quality control before taking it online.\nUse this URL if you ever want to make changes:' + jobEditUrl + '\n' +
                'For any question, please do not hesitate to email us at: support@launchafrica.io',
            html: `
            <h1>Your job post has been created!</h1>
            <h2>${job.title}</h2>
            <br />
            <p>We will quickly review it before taking it online.</p>
            <br />
            <p>Use the URL below if you ever want to make changes:</p>
            <a href=${jobEditUrl}>${jobEditUrl}</a>
            <br />
            <br />
            <p>For any question, please do not hesitate to email us at: support@launchafrica.io</p>
            <br />
            <p>Good luck on your recruitment,</p>
            <p>LaunchAfrica.io</p>
            </html>
        `,
        };
        mg.messages().send(data);
    }

    return admin.firestore().collection("companies")
        .where('name', '==', job.companyName.trim())
        .limit(1)
        .get()
        .then((querySnapshot) => {
            if(querySnapshot.empty) {
                const currentDate = new Date();
                return admin.firestore().collection('companies').add({
                    name: job.companyName.trim(),
                    website: job.companyWebsite.trim(),
                    key: job.companyName.trim().replace(/\s/g, "-").toLowerCase(),
                    description: job.companyDescription,
                    logo: job.companyLogo,
                    createdAt: currentDate,
                    createdTimestamp: Date.now(), // For sorting.
                });
            }
        });
});

exports.onJobPostDeleted = firestore.document('jobs/{id}').onDelete((snap) => {
    const index = algoliaClient.initIndex(process.env.ALGOLIA_INDEX_JOBS);
    return index.deleteObject(snap.id);
});

exports.onJobPostUpdated = firestore.document('jobs/{id}').onUpdate((change) => {
    const newData = change.after.data();
    if (newData.status === "active") {
        newData.objectID = change.after.id;
        const index = algoliaClient.initIndex(process.env.ALGOLIA_INDEX_JOBS);
        return index.saveObject(newData);
    } else {
        const index = algoliaClient.initIndex(process.env.ALGOLIA_INDEX_JOBS);
        return index.deleteObject(change.after.id);
    }
});
