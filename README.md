# Running a dev server
$ npm run dev

# Deploy with Firebase
### Sign in to Google
$ firebase login
### Initiate your project
$ firebase init
### When you're ready, deploy your web app
Put your static files (e.g. HTML, CSS, JS) in your app’s deploy directory (the default is 'public'). Then, run this command from your app’s root directory

$ firebase deploy

# Privacy Policy
https://app.termsfeed.com/download/7708934f-7581-46ab-a226-84fde6f8ca63

# Background
http://dragdropsite.github.io/waterpipe.js/

# Backup
https://console.cloud.google.com/firestore/import-export?project=africatech-7cf1b

Export: gcloud firestore export gs://launchafrica_backup

Import all by calling gcloud firestore import gs://[BUCKET_NAME]/[EXPORT_PREFIX]/ where [BUCKET_NAME] and [EXPORT_PREFIX] point to the location of your export files. For example - gcloud firestore import gs://exports-bucket/2017-05-25T23:54:39_76544/
